<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/log-out', function(){
	Auth::logout();

	return back();
});




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin-dashboard', 'AdminController@index')->name('adminindex');
Route::post('/addcondominium', 'AdminController@addcondominium');
Route::get('/condominium/{link}', 'AdminController@condominium');
Route::get('/management-dashboard', 'ManagementController@index')->name('managementindex');
Route::get('/apartments-management', 'ApartmentController@index');
Route::post('/registersection', 'ApartmentController@addsection');
Route::post('/addpartment', 'ApartmentController@addpartment');
Route::get('/apartment/{id}', 'ApartmentController@apartment');
Route::post('/newowner', 'ApartmentController@newowner');
Route::get('/charges', 'ApartmentController@charges');
Route::post('/addservicecharge', 'ApartmentController@servicecharge');
Route::post('/othercharges', 'ApartmentController@othercharges');
Route::get('/monthly-payments', 'PaymentController@monthlypayment');
Route::get('/monthly-consumption', 'PaymentController@monthlyconsumption');
Route::get('/monthly-reports', 'PaymentsController@monthlyreport');
Route::post('/addconsumptionrate', 'PaymentController@addmonthlyconsumption');
Route::get('/confirm-monthly-payment/{id}', 'PaymentController@confirmmonthlypayment');
Route::get('/monthly-default-list-current', 'PaymentController@currentmonthdefaultlist');
Route::get('/monthly-default-list-overall', 'PaymentController@overallmonthsdefaultlist');
Route::get('/apartments-list', 'ApartmentController@apartmentslist');
Route::get('/owners-directory', 'ApartmentController@directory');
Route::get('/service-charge-defaulters-list', 'PaymentController@servicechargedefaulterslist');
Route::get('/default-list/{id}', 'PaymentController@defaultlist');
Route::get('/monthly-charges-report', 'ReportsController@monthindex')->name('report');
Route::get('/monthly-charges-report/{id}', 'ReportsController@monthreport');
Route::get('/annual-payments', 'PaymentController@annualpayments')->name('annualreport');
Route::get('/annual-consumption', 'PaymentController@annualconsumption');
Route::get('/confirm-annual-payment/{id}', 'PaymentController@confirmannualpayment');
Route::get('/annual-charges-report', 'ReportsController@annualindex');
Route::get('/annual-charges-report/{id}', 'ReportsController@annualreport');
Route::get('/annual-default-list-current', 'PaymentController@currentyeardefaulterslist');
Route::get('/annual-default-list-overall', 'PaymentController@allyearsdefaulterslist');
Route::get('/annnualpayment-default-list/{id}', 'PaymentController@annualpaymentdefaultlist');
Route::get('/sms', 'AnnouncementController@sms');


