<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAmountToDoubleOnChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
                             
                           
           DB::statement(" ALTER TABLE charges MODIFY amount DOUBLE(10,2)");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('charges', function (Blueprint $table) {
                         $table->integer('amount')->change();
                    });             

    }
}
