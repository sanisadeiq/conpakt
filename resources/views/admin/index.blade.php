@extends('layouts.app')

@section('content')
<div class="container" style="margin-top: 40px">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Admin Dashboard</div>

                <div class="panel-body">
                   <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#newcondo">New Condominium</button>
                  @include('admin.partials.modal.newcondo')

                 <table class="table table-hover">
                    <thead>
                        <th>
                            Condominium Name
                        </th>
                        <th>
                            State
                        </th>
                        <th>
                            Contact Name
                        </th>
                    </thead>
                    <tbody>
                        @foreach($condominia as $condo)
                            <tr>
                                <td>
                                    <a href="{{ url('/condominium', [$condo->urllink]) }}">{{$condo->name}}</a>
                                </td>
                                <td>
                                    {{$condo->state}}
                                </td>
                                <td>
                                    {{$condo->contactname}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                     
                 </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
