 <div id="newcondo" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New Condominium</h4>
      </div>
      <div class="modal-body">
         <form class="form-horizontal" role="form" method="POST" action="{{ url('/addcondominium') }}">
                                {{ csrf_field() }}
        <div class="col-md-6">
          <h3 align="left" style="margin-bottom: 20px">Condominium Information</h3>
           <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                 
                   <div class="col-md-12">
                       <input id="name" type="text" class="form-control input-lg" name="name" value="{{ old('name') }}" required placeholder="Condominium Name" autofocus>
        
                          @if ($errors->has('name'))
                               <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                     </span>
                                        @endif
                        </div>
                      </div>
                       <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                               
                      
                                 <div class="col-md-6">
                                     
                                     <select id="state" type="text" class="form-control input-lg" name="state" value="{{ old('state') }}" required >
                                      <option>Select State</option>
                                       @foreach ($states as $state)
                                       <option value="{{$state}}">{{$state}}</option>
                                       @endforeach
                                     </select>
                      
                                        @if ($errors->has('state'))
                                             <span class="help-block">
                                                  <strong>{{ $errors->first('state') }}</strong>
                                                   </span>
                                                      @endif
                                      </div>
                           </div>
                         <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                  
                        
                                   <div class="col-md-12">
                                    <textarea id="address" class="form-control input-lg" name="address" value="{{ old('address') }}" required rows="5" placeholder="Address"></textarea>
                                      
                        
                                          @if ($errors->has('address'))
                                               <span class="help-block">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                     </span>
                                                        @endif
                                        </div>
                             
             </div>
        </div>

        <div class="col-md-6">
       <h3 align="left" style="margin-bottom: 20px">Contact Information</h3>

        <div class="form-group{{ $errors->has('contactname') ? ' has-error' : '' }}">
               
                  <div class="col-md-12">
                      <input id="contactname" type="text" class="form-control input-lg" name="contactname" value="{{ old('contactname') }}" required placeholder="Contact Person Name" >
       
                         @if ($errors->has('contactname'))
                              <span class="help-block">
                                   <strong>{{ $errors->first('contactname') }}</strong>
                                    </span>
                                       @endif
                       </div>
            </div>

             <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            
                       <div class="col-md-12">
                           <input id="email" type="email" class="form-control input-lg" name="email" value="{{ old('email') }}" required placeholder="Email Address" >
            
                              @if ($errors->has('email'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                         </span>
                                            @endif
                            </div>
                 </div>

                  <div class="form-group{{ $errors->has('officenumber') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input id="officenumber" type="number" class="form-control input-lg" name="officenumber" value="{{ old('officenumber') }}" required placeholder="Office Number">
                 
                                   @if ($errors->has('officenumber'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('officenumber') }}</strong>
                                              </span>
                                                 @endif
                                 </div>
                      </div>

                       <div class="form-group{{ $errors->has('mobilenumber') ? ' has-error' : '' }}">
                                
                                 <div class="col-md-12">
                                     <input id="mobilenumber" type="number" class="form-control input-lg" name="mobilenumber" value="{{ old('mobilenumber') }}" required placeholder="Mobile Number" >
                      
                                        @if ($errors->has('mobilenumber'))
                                             <span class="help-block">
                                                  <strong>{{ $errors->first('mobilenumber') }}</strong>
                                                   </span>
                                                      @endif
                                      </div>
                           </div>

        </div>
        
        <div class="form-group">
             <div class="col-md-12">
               <div class="col-md-8 ">
            <button type="submit" class="btn btn-primary">
                           Add
             </button>
        
                                       
                                    </div>
             </div>
          </div>
           </form>
        
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>