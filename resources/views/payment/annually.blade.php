@extends('layouts.app')

@section('content')

   
       <div class="col-md-9" style="margin: 0px;padding: 0px;overflow-y: scroll;height: 93vh">
            <div class="panel panel-default" style="margin: 0px;">
             
                <div class="panel-body" style="margin-top: 0px">
                  <h3 align="left" style="margin-top: 0px!important">{{$date->startOfMonth()->subYear()->format('Y')}} Payments Status </h3> 
                 
                  <div class="col-md-12" style="margin-top: 50px">
                  	@if(Route::currentRouteName() !== 'annualreport')
                  		<div class="btn-group pull-right">
  <button type="button" class="btn btn-default dropdown-toggle pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Select Year <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    @foreach($years->unique('created_at') as $year)
    <li role="separator" class="divider"></li>
    <li><a href="{{ url('/annual-charges-report', [$year->id]) }}">{{$year->created_at->subYear()->format('Y')}}</a></li>
     <li role="separator" class="divider"></li>
    @endforeach
</ul>
</div>
@endif

<table class="table table-hover table-bordered text-center" style="margin-top: 60px">
	<thead class="text-center">
		<th class="text-center">
			#
		</th>
		<th class="text-center">
			Apartment Number
		</th>
		@foreach(App\Annualamount::currentyearcharges() as $charge)
		<th class="text-center">
			{{$charge->chargetype}}
		</th>
		@endforeach
	</thead>
	<tbody>
		@foreach(Auth::user()->condo as $apartment)
			<tr>

				<td>
					{{$loop->index + 1}}
				</td>
				<td>
					<a href="{{ url('/apartment', [$apartment->id]) }}">{{$apartment->apartmentnumber}}</a>
				</td>
		@foreach(App\Annualamount::currentyearcharges() as $charge)
		<td >
			
				{!! App\Helpers\annualamounts::calculateamount($apartment->id, $charge) !!}
			
		</td>
		@endforeach

			</tr>
		@endforeach

	</tbody>
	
</table>

             
                  </div>

                 
                </div>
            </div>
        </div>
   

@endsection

