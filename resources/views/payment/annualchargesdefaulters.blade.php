@extends('layouts.app')

@section('content')

   
       <div class="col-md-9" style="margin: 0px;padding: 0px;overflow-y: scroll;height: 93vh">
            <div class="panel panel-default" style="margin: 0px;">
             
                <div class="panel-body" style="margin-top: 0px">
                  <h3 align="left" style="margin-top: 0px!important">{{$title}}  </h3>

                  <div class="col-md-12" style="margin-top: 40px">
                  	<table class="table table-hover table-bordered" style="text-align: center">
                  	<thead>
                  		<th style="text-align: center">
                  			#
                  		</th>
                  		<th style="text-align: center">
                  			Apartment Number
                  		</th>
		              @foreach($currentyearcharges as $charge)
						<th class="text-center">
							{{$charge->chargetype}}
						</th>
					@endforeach
                  	</thead>
                  	   <tbody>
                          @foreach($defaulters->unique('aid') as $debtor)
                          <tr>
                                <td>
                                      {{$loop->index + 1}}
                                </td>
                                <td>
                                     <a href="{{ url('/apartment', [$debtor->aid]) }}">{{$debtor->apartmentnumber}}</a>
                                </td>
                                @foreach($currentyearcharges as $charge)
								<td >
									
										{!! App\Helpers\annualamounts::calculateamount($debtor->aid, $charge) !!}
									
								</td>
								@endforeach
                          </tr>
                          @endforeach
                       </tbody>
                  	
                  </table>
                  </div>
                 
                 
                </div>
            </div>
        </div>
   

@endsection

