@extends('layouts.app')

@section('content')

   
       <div class="col-md-9" style="margin: 0px;padding: 0px;overflow-y: scroll;height: 93vh">
            <div class="panel panel-default" style="margin: 0px;">
             
                <div class="panel-body" style="margin-top: 0px">
                  <h3 align="left" style="margin-top: 0px!important">{{ $defaulterslist->unique('chargetype')->first()->chargetype }} Defaulters List  </h3>

                  <div class="col-md-12" style="margin-top: 40px">
                  	<table class="table table-hover table-bordered" style="text-align: center">
                  	<thead>
                  		<th style="text-align: center">
                  			#
                  		</th>
                  		<th style="text-align: center">
                  			Apartment Number
                  		</th>
		                  <th>
                      No of Default Months  
                      </th>
                      <th>
                        Total Amount 
                      </th>
                  	</thead>
                  	   <tbody>
                          @foreach($defaulterslist->unique('aid') as $defaulter)
                          <tr>
                                <td>
                                      {{$loop->index + 1}}
                                </td>
                                <td>
                                     <a href="{{ url('/apartment', [$defaulter->aid]) }}">{{$defaulter->apartmentnumber}}</a>
                                </td>
                                <td>
                                  {{App\Monthpayment::defaultmonthcount($defaulter->aid, $defaulter->cid)}}
                                </td>
                                <td>
                                  RM {{App\Monthpayment::defaultamount($defaulter->aid, $defaulter->cid)}}
                                </td>
                          </tr>
                          @endforeach
                       </tbody>
                  	
                  </table>
                  </div>
                 
                 
                </div>
            </div>
        </div>
   

@endsection

