@extends('layouts.app')

@section('content')

   
       <div class="col-md-9" style="margin: 0px;padding: 0px;overflow-y: scroll;height: 93vh">
            <div class="panel panel-default" style="margin: 0px;">
             
                <div class="panel-body" style="margin-top: 0px">
                  <h3 align="left" style="margin-top: 0px!important">{{$date->startOfMonth()->subMonth()->format('F')}} Payments Status </h3> 
                 
                  <div class="col-md-12" style="margin-top: 50px">
                  	@if(Route::currentRouteName() == 'report')
                  		<div class="btn-group pull-right">
  <button type="button" class="btn btn-default dropdown-toggle pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Select Month <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    @foreach($months->unique('created_at') as $month)
    <li role="separator" class="divider"></li>
    <li><a href="{{ url('/monthly-charges-report', [$month->id]) }}">{{$month->created_at->subMonth()->format('F Y')}}</a></li>
    @endforeach
</ul>
</div>
@endif

<table class="table table-hover table-bordered text-center" style="margin-top: 60px">
	<thead class="text-center">
		<th class="text-center">
			#
		</th>
		<th class="text-center">
			Apartment Number
		</th>
		@foreach(App\Monthamount::currentmonthcharges() as $charge)
		<th class="text-center">
			{{$charge->chargetype}}
		</th>
		@endforeach
	</thead>
	<tbody>
		@foreach(Auth::user()->condo as $apartment)
			<tr>

				<td>
					{{$loop->index + 1}}
				</td>
				<td>
					<a href="{{ url('/apartment', [$apartment->id]) }}">{{$apartment->apartmentnumber}}</a>
				</td>
		@foreach(App\Monthamount::currentmonthcharges() as $charge)
		<td >
			
				{!! App\Helpers\amounts::calculateamount($apartment->id, $charge) !!}
			
		</td>
		@endforeach

			</tr>
		@endforeach

	</tbody>
	
</table>

             
                  </div>

                 
                </div>
            </div>
        </div>
   

@endsection

