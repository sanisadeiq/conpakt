@extends('layouts.app')

@section('content')
<div class="col-md-9" style="margin: 0px;padding: 0px;overflow-y: scroll;height: 93vh">
            <div class="panel panel-default" style="margin: 0px;">
   
             
                <div class="panel-body" style="margin-top: 0px">
                  <h3 align="left" style="margin-top: 3px!important">{{$date->startOfMonth()->subMonth()->format('F')}} Consumption Rate  </h3>
                  <div class="col-md-12" style="margin-top: 50px">
                  	@foreach(Auth::user()->sections as $section)
                  	<div class="col-md-12">
                  		<h4 align="left">{{$section->sectionname}}</h4>
                               <form class="form-horizontal" role="form" method="POST" action="{{ url('/addconsumptionrate') }}">
                                                      {{ csrf_field() }}
                              
                  		<table class="table table-hover table-bordered">
                  			<thead>
                  				<th>
                  					#
                  				</th>
                  				<th>
                  					Apartment Number
                  				</th>
                  			@foreach(App\Monthamount::currentmonthcharges()->whereNotIn('chargetype', ['Service Charge']) as $charge)
                  			<th>
                  				{{$charge->chargetype}}
                  			</th>
                        <th>
                          Amount Consumed
                        </th>
                  			@endforeach

                  			</thead>
                  			<tbody>
                  				@foreach($section->apartments as $apartment)
                  					<tr>
                  						<td>
                  							{{$loop->index + 1}}
                  						</td>
                  						<td>
                  							{{$apartment->apartmentnumber}}
                  						</td>
                                          @foreach(App\Monthamount::currentmonthcharges()->whereNotIn('chargetype', ['Service Charge']) as $charge)
                                    <td>
                                         @if(is_null(App\Consumptionrate::checkconsumptionrate($apartment->id, $charge->id)))
                                          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                         
                                                    <div class="col-md-8">
                                                      <input type="hidden" name="apartmentid[]" value="{{$apartment->id}}">
                                                      <input type="hidden" name="chargeid[]" value="{{$charge->id}}">
                                                        <input id="name" type="number" class="form-control" name="amount[]" value="{{ old('name') }}" required >
                                         
                                                           @if ($errors->has('name'))
                                                                <span class="help-block">
                                                                     <strong>{{ $errors->first('name') }}</strong>
                                                                      </span>
                                                                         @endif
                                                         </div>
                                              </div>
                                          @else
                                          RM {{App\Consumptionrate::checkconsumptionrate($apartment->id, $charge->id)->consumptioncost}}
                                          @endif

                                    </td>
                                    @endforeach
                  					</tr>
                  				@endforeach
                  			</tbody>
                  			
                  		</table>
                               <div class="form-group">
                                   <div class="col-md-4 pull-right">
                                  <button type="submit" class="btn btn-primary pull-right">
                                                 Add
                                   </button>
                              
                                                             
                                                          </div>
                                </div>
                                 </form>
                  		
                  	</div>
                  	@endforeach

                               
                  </div>

                 
                </div>
            </div>
        </div>
   

@endsection

