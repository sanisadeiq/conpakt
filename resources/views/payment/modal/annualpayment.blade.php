<!-- Modal -->
<div id="annualpayment{{$charge->mid}}" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Payment Confirmation</h4>
      </div>
      <div class="modal-body">
        <p>By clicking on the confirm button below, you hereby confirm that the apartment owner has made payment for the following service and amount.</p>
        <label>Charge Type</label> {{$charge->chargetype}} <br/>
        <label>Amount</label>  {!! App\Helpers\annualamounts::calculateamountwithoutstatus($id, $charge) !!} <br/>
        <a href="{{ url('/confirm-annual-payment', [$charge->mid]) }}"><button class="btn btn-primary">Confirm Payment</button></a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>