@extends('layouts.app')

@section('content')

   
       <div class="col-md-9" style="margin: 0px;padding: 0px;overflow-y: scroll;height: 93vh">
            <div class="panel panel-default" style="margin: 0px;">
             
                <div class="panel-body" style="margin-top: 0px">
                  <h3 align="left" style="margin-top: 0px!important">Overall Defaulters List  </h3>

                  <div class="col-md-12" style="margin-top: 40px">
                  	<table class="table table-hover table-bordered" style="text-align: center">
                  	<thead>
                  		<th style="text-align: center">
                  			#
                  		</th>
                  		<th style="text-align: center">
                  			Apartment Number
                  		</th>
          		              @foreach(App\Monthamount::currentmonthcharges() as $charge)
          						<th class="text-center">
          							{{$charge->chargetype}}
          						</th>
          					@endforeach
                    <th style="text-align: center">
                      Month
                    </th>
                  	</thead>
                  	   <tbody>
                          @foreach($debtorlist->unique('aid')->where('status', 0) as $debtor)
                          <tr>
                                <td>
                                      {{$loop->index + 1}}
                                </td>
                                <td>
                                     <a href="{{ url('/apartment', [$debtor->aid]) }}">{{$debtor->apartmentnumber}}</a>
                                </td>
                                @foreach(App\Monthamount::currentmonthcharges() as $charge)
              								<td >
              									
              										{!! App\Helpers\amounts::calculateamount($debtor->aid, $charge) !!}
              									
              								</td>
								              @endforeach
                              <td>
                                    {{$debtor->created_at->subMonth()->format('F Y')}}
                                </td>
                          </tr>
                          @endforeach
                       </tbody>
                  	
                  </table>
                  </div>
                 
                 
                </div>
            </div>
        </div>
   

@endsection

