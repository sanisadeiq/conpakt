<!DOCTYPE html>
<html>
<head>
	<title>Invoice</title>
</head>
<body>
<h1 align="center">{{Auth::user()->name}}</h1>
<h3 align="center"> {{Auth::user()->management->address}}, {{Auth::user()->management->state}}</h3>
<h6 align="center">{{Auth::user()->management->officenumber}}</h6>

<div style="margin-top: 60px">
	{{$monthlypayment->name}} <br/>
	{{$monthlypayment->apartmentnumber}} <br/>
	{{$monthlypayment->contactaddress}} <br/>
	{{$currentdate->format('d-m-y')}} <br/>
</div>

<div style="margin-top: 60px">
	<h4 align="center">PAYMENT INVOICE</h4>

	<table width="100%" style="text-align: center;margin-top: 20px">
		<tr>
			<td>
				CHARGE TYPE
			</td>
			<td>
				AMOUNT PAID
			</td>
		</tr>
		<tr>
			<td>
				{{$monthlypayment->chargetype}}
			</td>
			<td>
				{!! App\Helpers\annualamounts::calculateamountwithoutstatus($monthlypayment->id, $monthlypayment) !!}
			</td>
		</tr>
	</table>

	
</div>
<div style="position: fixed;bottom: 0;width: 100%;text-align: center;background-color: black;color: white;margin:0px;padding: 0px">
		POWERED BY CONPAKT
	</div>
</body>

</html>