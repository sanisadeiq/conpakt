@extends('layouts.app')

@section('content')

        <div class="col-md-9" style="margin: 0px;padding: 0px;overflow-y: scroll;height: 93vh">
            <div class="panel panel-default">
              

                <div class="panel-body">
                    <h3 align="left" style="margin-top: 3px!important">Charges </h3>
                        @if(Auth::user()->charges->where('chargetype', 'Service Charge')->count() == 0)
                         <div class="col-md-8 col-md-offset-2">
                        <h4 align="center" style="margin-bottom: 20px">ADD SERVICE CHARGE</h4>
                          <form class="form-horizontal" role="form" method="POST" action="{{ url('/addservicecharge') }}">
                                          {{ csrf_field() }}
                   <div class="form-group{{ $errors->has('cost') ? ' has-error' : '' }}">
                            <label for="cost" class="col-md-4 control-label">Amount/Square Metre</label>
                  
                             <div class="col-md-5">
                                 <input id="cost" type="number" class="form-control" name="cost" value="{{ old('cost') }}" required >
                  
                                    @if ($errors->has('cost'))
                                         <span class="help-block">
                                              <strong>{{ $errors->first('cost') }}</strong>
                                               </span>
                                                  @endif
                                  </div>
                       </div>

                        <div class="form-group{{ $errors->has('billingtype') ? ' has-error' : '' }}">
                                 <label for="billingtype" class="col-md-4 control-label">Billing Type</label>
                       
                                  <div class="col-md-8">
                                     

                                <label class="radio-inline"><input type="radio" id="billingtype"  name="billingtype" value="1">Monthly</label>
                                <label class="radio-inline"><input type="radio" id="billingtype"  name="billingtype" value="2">Annually</label>
                       
                                         @if ($errors->has('billingtype'))
                                              <span class="help-block">
                                                   <strong>{{ $errors->first('billingtype') }}</strong>
                                                    </span>
                                                       @endif
                                       </div>
                            </div>

                             <div class="form-group{{ $errors->has('chargetype') ? ' has-error' : '' }}">
                                 <label for="chargetype" class="col-md-4 control-label">Charge Type</label>
                       
                                  <div class="col-md-8">
                                     

                                <label class="radio-inline"><input type="radio" id="chargetype" name="chargetype" value="1">Fixed</label>
                                <label class="radio-inline"><input type="radio" id="chargetype"  name="chargetype" value="2">Per Consumption</label>
                       
                                         @if ($errors->has('chargetype'))
                                              <span class="help-block">
                                                   <strong>{{ $errors->first('chargetype') }}</strong>
                                                    </span>
                                                       @endif
                                       </div>
                            </div>


                  
                  <div class="form-group">
                       <div class="col-md-8 col-md-offset-4">
                      <button type="submit" class="btn btn-primary">
                                     Add
                       </button>
                  
                                                 
                                              </div>
                    </div>
                     </form>
                  </div>
                  @endif

                  <div class="col-md-8 col-md-offset-2">
                       <h4 align="center" style="margin-bottom: 20px">OTHER CHARGES</h4>
                          <form class="form-horizontal" role="form" method="POST" action="{{ url('/othercharges') }}">
                                          {{ csrf_field() }}
                     <div class="form-group{{ $errors->has('chargetype') ? ' has-error' : '' }}">
                              <label for="chargetype" class="col-md-4 control-label">Charge Type</label>
                    
                               <div class="col-md-6">
                                   <input id="chargetype" type="text" class="form-control" name="chargetype" value="{{ old('chargetype') }}" required >
                    
                                      @if ($errors->has('chargetype'))
                                           <span class="help-block">
                                                <strong>{{ $errors->first('chargetype') }}</strong>
                                                 </span>
                                                    @endif
                                    </div>
                         </div>
                   <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                            <label for="amount" class="col-md-4 control-label">Amount</label>
                  
                             <div class="col-md-6">
                                 <input id="amount" type="text" class="form-control" name="amount" value="{{ old('amount') }}" required >
                  
                                    @if ($errors->has('amount'))
                                         <span class="help-block">
                                              <strong>{{ $errors->first('amount') }}</strong>
                                               </span>
                                                  @endif
                                  </div>
                       </div>
                        <div class="form-group{{ $errors->has('billingtype') ? ' has-error' : '' }}">
                                 <label for="billingtype" class="col-md-4 control-label">Billing Type</label>
                       
                                  <div class="col-md-8">
                                     


<label class="radio-inline"><input type="radio" id="billingtype"  name="billingtype" value="1">Monthly</label>
<label class="radio-inline"><input type="radio" id="billingtype"  name="billingtype" value="2">Annually</label>
                       
                                         @if ($errors->has('billingtype'))
                                              <span class="help-block">
                                                   <strong>{{ $errors->first('billingtype') }}</strong>
                                                    </span>
                                                       @endif
                                       </div>
                            </div>


                             <div class="form-group{{ $errors->has('chargingtype') ? ' has-error' : '' }}">
                                 <label for="chargingtype" class="col-md-4 control-label">Charging Type</label>
                       
                                  <div class="col-md-8">
                                     

                                <label class="radio-inline"><input type="radio" id="chargingtype" name="chargingtype" value="1">Fixed</label>
                                <label class="radio-inline"><input type="radio" id="chargingtype"  name="chargingtype" value="2">Per Consumption</label>
                       
                                         @if ($errors->has('chargetype'))
                                              <span class="help-block">
                                                   <strong>{{ $errors->first('chargetype') }}</strong>
                                                    </span>
                                                       @endif
                                       </div>
                            </div>

 
                  
                  <div class="form-group">
                       <div class="col-md-8 col-md-offset-4">
                      <button type="submit" class="btn btn-primary">
                                     Add
                       </button>
                  
                                                 
                                              </div>
                    </div>
                     </form>
                  </div>

                  <div class="col-md-12" style="margin-top: 40px">
                      <table class="table table-hover table-bordered">
                        <thead>
                            <th>
                              #
                            </th>
                            <th>
                                Charge Type
                            </th>
                            <th>
                                Price Per Unit
                            </th>
                            <th>
                                Billing Type
                            </th>
                            <th>
                              Charging Type
                            </th>
                        </thead>
                        <tbody>
                            @foreach(Auth::user()->charges as $charge)
                                <tr>
                                    <td>
                                      {{$loop->index + 1}}
                                    </td>
                                    <td>
                                        {{$charge->chargetype}}
                                    </td>
                                    <td>
                                        {{$charge->amount}}
                                    </td>
                                    <td>
                                        {{$charge->billingtype}}
                                    </td>
                                    <td>
                                      {{$charge->chargingtype}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                          
                      </table>
                  </div>
                  
                    

                </div>
            </div>
        </div>
    
@endsection
