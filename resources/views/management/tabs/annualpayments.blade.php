<h4 align="left">Annual Payments</h4>
                    <div class="col-md-12" style="margin-top: 15px">
                      <div class="col-md-4">
                        <div class="panel panel-default" align="center">
                          <div class="panel-heading">
                                                Number of Charge Types
                                          </div>
                                          <div class="panel-body">
                            <h2>{{$annualchargescount}}</h2>
                          </div>
                        </div>
                        
                      </div>
                      <div class="col-md-4">
                        <div class="panel panel-default" align="center">
                                <div class="panel-heading">Number of Apartments  </div>
                                          <div class="panel-body">
                            <h2>{{$apartmentcount}}</h2>
                          </div>
                        </div>
                        
                      </div>
                      <div class="col-md-4">
                        <div class="panel panel-default" align="center">
                                          <div class="panel-heading">
                                                Current Year Expected Income
                                          </div>
                          <div class="panel-body">
                            <h2>RM {{$annualchargescurrentyearexpectedincome}}</h2>
                          </div>
                          
                          
                        </div>
                        
                      </div>

                      <div class="col-md-12">
                                    <div class="col-md-6">
                                          <h4 align="left">Current Year Income Distribution</h4>
                                        <div id="chart-div0"></div>
<?= Lava::render('PieChart', 'annualchargesdistribution', 'chart-div0') ?>
                                    </div>
                                    <div class="col-md-6">
                                         <h4 align="left">Amount Paid Analysis</h4>
                                          <div id="perf_div0"></div>
<?= Lava::render('ColumnChart', 'paymentdistribution', 'perf_div0') ?>
                                    </div>
                                      <div class="col-md-6">
                                         <h4 align="left">Current Year Payment Analysis</h4>
                                          <div id="perf_div00"></div>
<?= Lava::render('PieChart', 'annualpaymentanalysis', 'perf_div00') ?>
                                    </div>

                                    <div class="col-md-6">
                                         <h4 align="left">Payment Analysis</h4>
                                          <div id="perf_div000"></div>
<?= Lava::render('BarChart', 'annualNumbers', 'perf_div000') ?>
                                    </div>
                              </div>
                                <div class="col-md-12" style="margin-top: 30px">
                                    <div class="col-md-6">
                                          <h4 align="left">Current Year Defaulters List</h4>

                                          <table class="table table-hover table-bordered" style="margin-top: 20px" align="center" width="80%">
                                                <thead>
                                                      <th>
                                                            #
                                                      </th>
                                                      <th>
                                                            Apartment Number
                                                      </th>
                                                </thead>
                                                <tbody>
                                                  @foreach($annualchargescurrentyeardefaulterslist->unique('aid')->take(5) as $debtor)
                                                  <tr>
                                                        <td>
                                                              {{$loop->index + 1}}
                                                        </td>
                                                        <td>
                                                             <a href="{{ url('/apartment', [$debtor->aid]) }}">{{$debtor->apartmentnumber}}</a>
                                                        </td>
                                                  </tr>
                                                  @endforeach
                                                </tbody>
                                               
                                          </table>
                                           <span class="pull-right">
                                             <a href="{{ url('annual-default-list-current') }}">View complete current year defaulters list</a>
                                           </span>
                                    </div>
                                    <div class="col-md-6">
                                          <h4 align="left">Overall Annual Payments Defaulters List</h4>

                                            <table class="table table-hover table-bordered" style="margin-top: 20px" align="center" width="80%">
                                                <thead>
                                                      <th>
                                                            #
                                                      </th>
                                                      <th>
                                                            Apartment Number
                                                      </th>
                                                </thead>
                                                  <tbody>
                                                  @foreach($annualchargesdefaulterslist->unique('aid')->take(5) as $debtor)
                                                  <tr>
                                                        <td>
                                                              {{$loop->index + 1}}
                                                        </td>
                                                        <td>
                                                              <a href="{{ url('/apartment', [$debtor->aid]) }}">{{$debtor->apartmentnumber}}</a>
                                                        </td>
                                                  </tr>
                                                  @endforeach
                                                </tbody>
                                          </table>
                                          <span class="pull-right">
                                            <a href="{{ url('/annual-default-list-overall') }}">View complete overall annual defaulters list</a>
                                          </span>
                                    </div>
                                    
                              </div>
                </div>
                                   <div class="col-md-12" style="margin-top: 20px;margin-right: 0px;margin-right: 0px">
                                     <h4 align="left">Individual Charge Payment Analysis</h4>

                                       @foreach($annualpayments as $payment)
                                         @include('management.partials.annualdynamiccharts')
                                         @php
                                         $annualdiv = "annualdiv".$payment->aid;
                                        $totaldiv = "totaldiv".$payment->aid;
                                      $annualcurrentchart = "currentchart".$payment->aid;
                                      $annualgeneralchart = "generalchart".$payment->aid;

                                         @endphp
                                      <div class="col-md-12">
                                        <h4 align="left">{{$payment->chargetype}}</h4>
                                        <div class="col-md-6">
                                          <div class="col-md-12">
                                            <h4 align="left">Current Year Total</h4>
                                            <h1 align="left"> RM {{ App\Annualamount::currentyearchargeexpectedincome($payment->aid) }}</h1>
                                          </div>
                                          <div class="col-md-12">
                                            <h4 align="left">Current Year Payment Rate</h4>
                                           
                                          
                                           

                                             <div id="{{$annualdiv}}"></div>
                                           
                                            <?= Lava::render('PieChart', $annualcurrentchart, $annualdiv) ?>
                                          </div>
                                        </div>
                                         <div class="col-md-6">
                                          <div class="col-md-12">
                                            <h4 align="left">Total Expected Amount</h4>
                                            <h1 align="left"> RM {{ App\Annualamount::totalexpectedincome($payment->aid) }}</h1>
                                          </div>
                                          <div class="col-md-12">
                                            <h4 align="left">General Payment Rate</h4>
                                             <div id="{{$totaldiv}}"></div>
                                            
                                            <?= Lava::render('PieChart', $annualgeneralchart, $totaldiv) ?>
                                            <span class="pull-right">
                                              <a href="{{ url('/annnualpayment-default-list',[$payment->aid]) }}">
                                                View complete list of {{$payment->chargetype}} defaulters
                                              </a>
                                             </span>
                                          </div>
                                        </div>
                                      </div>

                                    @endforeach
                                   </div>
