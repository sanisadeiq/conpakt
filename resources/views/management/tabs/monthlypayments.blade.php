 <div class="col-md-12" style="margin-top: 20px;margin-right: 0px;margin-right: 0px">
                  	<h4 align="left">Monthly Payments</h4>
                  	<div class="col-md-12" style="margin-top: 15px">
                  		<div class="col-md-4">
                  			<div class="panel panel-default" align="center">
                  				<div class="panel-heading">
                                                Number of Charge Types
                                          </div>
                                          <div class="panel-body">
                  					<h2>{{$chargecount}}</h2>
                  				</div>
                  			</div>
                  			
                  		</div>
                  		<div class="col-md-4">
                  			<div class="panel panel-default" align="center">
                  		          <div class="panel-heading">Number of Apartments  </div>
                                          <div class="panel-body">
                  					<h2>{{$apartmentcount}}</h2>
                  				</div>
                  			</div>
                  			
                  		</div>
                  		<div class="col-md-4">
                  			<div class="panel panel-default" align="center">
                                          <div class="panel-heading">
                                              Current Month Expected Income
                                          </div>
                  				<div class="panel-body">
                  					<h2>RM {{$expectedincome}}</h2>
                  				</div>
                  				
                  				
                  			</div>
                  			
                  		</div>
                              <div class="col-md-12">
                                    <div class="col-md-6">
                                          <h4 align="left">Current Month Income Distribution</h4>
                                        <div id="chart-div"></div>
<?= Lava::render('PieChart', 'IMDB', 'chart-div') ?>
                                    </div>
                                    <div class="col-md-6">
                                         <h4 align="left">Amount Paid Analysis</h4>
                                          <div id="perf_div"></div>
<?= Lava::render('ColumnChart', 'Finances', 'perf_div') ?>
                                    </div>
                                      <div class="col-md-6">
                                         <h4 align="left">Current Month Payment Analysis</h4>
                                          <div id="perf_div1"></div>
<?= Lava::render('PieChart', 'paymentanalysis', 'perf_div1') ?>
                                    </div>

                                    <div class="col-md-6">
                                         <h4 align="left">Payment Analysis</h4>
                                          <div id="perf_div2"></div>
<?= Lava::render('BarChart', 'Numbers', 'perf_div2') ?>
                                    </div>
                              </div>
                              <div class="col-md-12" style="margin-top: 30px">
                                    <div class="col-md-6">
                                          <h4 align="left">Current Month Defaulters List</h4>

                                          <table class="table table-hover table-bordered" style="margin-top: 20px" align="center" width="80%">
                                                <thead>
                                                      <th>
                                                            #
                                                      </th>
                                                      <th>
                                                            Apartment Number
                                                      </th>
                                                </thead>
                                                <tbody>
                                                  @foreach($debtorlist->unique('aid')->where('status', 0)->take(5) as $debtor)
                                                  <tr>
                                                        <td>
                                                              {{$loop->index + 1}}
                                                        </td>
                                                        <td>
                                                             <a href="{{ url('/apartment', [$debtor->aid]) }}">{{$debtor->apartmentnumber}}</a>
                                                        </td>
                                                  </tr>
                                                  @endforeach
                                                </tbody>
                                               
                                          </table>
                                           <span class="pull-right">
                                             <a href="{{ url('monthly-default-list-current') }}">View complete current month defaulters list</a>
                                           </span>
                                    </div>
                                    <div class="col-md-6">
                                          <h4 align="left">Overall Monthly Payments Defaulters List</h4>

                                            <table class="table table-hover table-bordered" style="margin-top: 20px" align="center" width="80%">
                                                <thead>
                                                      <th>
                                                            #
                                                      </th>
                                                      <th>
                                                            Apartment Number
                                                      </th>
                                                </thead>
                                                  <tbody>
                                                  @foreach($overalldebtorlist->unique('aid')->where('status', 0)->take(5) as $debtor)
                                                  <tr>
                                                        <td>
                                                              {{$loop->index + 1}}
                                                        </td>
                                                        <td>
                                                              <a href="{{ url('/apartment', [$debtor->aid]) }}">{{$debtor->apartmentnumber}}</a>
                                                        </td>
                                                  </tr>
                                                  @endforeach
                                                </tbody>
                                          </table>
                                          <span class="pull-right">
                                            <a href="{{ url('/monthly-default-list-overall') }}">View complete overall month defaulters list</a>
                                          </span>
                                    </div>
                                    
                              </div>
                  	</div>
                  </div>
                   <div class="col-md-12" style="margin-top: 20px;margin-right: 0px;margin-right: 0px">
                        <h4 align="left">Individual Charge Payment Analysis</h4>
                        <div class="col-md-12">
                          <h4 align="left">Service Charge</h4>
                          <div class="col-md-6">
                            <div class="col-md-12">
                              <h4 align="left">Current Month Total Service Charge </h4>          
                              <div >
                                <h1 align="left"> RM {{$monthtotalservicechargae}}</h1>
                              </div>
                        </div>
                           
                            <div class="col-md-12">
                              <h4 align="left">Current Month Payment Rate</h4>
                              <div id="perf_div5"></div>
                                <?= Lava::render('PieChart', 'servicechargepayment', 'perf_div5') ?>
                            </div>
                          </div>
                            <div class="col-md-6">
                               <div class="col-md-12">
                              <h4 align="left">Total Expected Service Charge </h4>          
                              <div >
                                <h1 align="left"> RM {{$totalservicecharge}}</h1>
                              </div>
                        </div>
                           <div class="col-md-12">
                              <h4 align="left">General Payment Rate</h4>
                              <div id="perf_div6"></div>
                                <?= Lava::render('PieChart', 'totalservicecharge', 'perf_div6') ?>
                          </div>
                          <span class="pull-right">
                            <a href="{{ url('/service-charge-defaulters-list') }}">
                               View Complete List of Service Charge Defaulters
                            </a>
                        </span>
                           </div>
                          
                        </div>
                        @foreach($monthpayments as $payment)
                          @php
                                 $currentpaymentchart = "currentdiv".$payment->aid;
                                  $generalpaymentchart = "generaldiv".$payment->aid;
                                  $currentchart = "currentchart".$payment->aid;
$generalchart = "generalchart".$payment->aid;
                                    @endphp
                          <div class="col-md-12">
                            <h4 align="left">{{$payment->chargetype}}</h4>
                            <div class="col-md-6">
                              <div class="col-md-12">
                                <h4 align="left">Current Month Total</h4>
                                <h1 align="left"> RM {{ App\Charges::specificcharge($payment->aid) }}</h1>
                              </div>
                                @include('management.partials.monthlydynamiccharts')

                              <div class="col-md-12">
                                <h4 align="left">Current Month Payment Rate</h4>
                                 <div id="{{$currentpaymentchart}}"></div>

                               

              
                               
                                  
                                <?= Lava::render('PieChart', $currentchart, $currentpaymentchart) ?>
                              </div>
                            </div>
                             <div class="col-md-6">
                              <div class="col-md-12">
                                <h4 align="left">Total Expected Amount</h4>
                                <h1 align="left"> RM {{ App\Consumptionrate::specificchargetotal($payment->aid) }}</h1>
                              </div>
                              <div class="col-md-12">
                                <h4 align="left">General Payment Rate</h4>
                                 <div id="{{$generalpaymentchart}}"></div>
                                
                                <?= Lava::render('PieChart', $generalchart, $generalpaymentchart) ?>
                                <span class="pull-right">
                                  <a href="{{ url('/default-list',[$payment->id]) }}">
                                    View complete list of {{$payment->chargetype}} defaulters
                                  </a>
                                 </span>
                              </div>
                            </div>
                          </div>
                        @endforeach
                        
                  </div>