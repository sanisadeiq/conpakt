@extends('layouts.app')

@section('content')

        <div class="col-md-9" style="margin: 0px;padding: 0px;overflow-y: scroll;height: 93vh">
            <div class="panel panel-default">
              

                <div class="panel-body">
                    <h3 align="left" style="margin-top: 3px!important">SMS </h3>
                       
                    <div class="col-md-8 col-md-offset-2" style="align-content: center;">
                      <h3 align="center">New Announcement</h3>
                       <form class="form-horizontal" role="form" method="POST" action="{{ url('/sendsms') }}" style="margin-top: 30px">
                                              {{ csrf_field() }}
                       <div class="form-group{{ $errors->has('recipient') ? ' has-error' : '' }}">
                              
                      
                           <div class="col-md-8 col-md-offset-3">
                               <input id="recipient" type="text" class="form-control" name="recipient" value="{{ old('recipient') }}" required placeholder="Recipients">
                      
                                  @if ($errors->has('recipient'))
                                       <span class="help-block">
                                            <strong>{{ $errors->first('recipient') }}</strong>
                                             </span>
                                                @endif
                                </div>
                      </div>
                       <div class="form-group{{ $errors->has('announcement') ? ' has-error' : '' }}">
                               
                           <div class="col-md-8 col-md-offset-3">
                             
                              <text-area></text-area>
                      
                                  @if ($errors->has('announcement'))
                                       <span class="help-block">
                                             </span>
                                                @endif
                                </div>
                      </div>
                      
                      <div class="form-group">
                           <div class="col-md-8 col-md-offset-3">
                          <button type="submit" class="btn btn-primary">
                                         SEND
                           </button>
                      
                                                     
                                                  </div>
                        </div>
                         </form>
                      
                      
                    </div>
                </div>
            </div>
        </div>
    
@endsection
