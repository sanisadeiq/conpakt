@php
use Khill\Lavacharts\Lavacharts;
use App\Charges;
use App\Annualamount;
$lava = new Lavacharts; 

$annualcurrentchart = "currentchart".$payment->aid;
$annualgeneralchart = "generalchart".$payment->aid;

		$reasons = $lava->DataTable();

		$reasons->addStringColumn('Paid')
		        ->addNumberColumn('Percent')
		        ->addRow(['Paid',  Annualamount::currentyearchargepaidincome($payment->aid) ])
		        ->addRow(['Unpaid', Annualamount::currentyearchargeexpectedincome($payment->aid) - Annualamount::currentyearchargepaidincome($payment->aid)]);
		       
		 $Chart = \Lava::PieChart($annualcurrentchart, $reasons, [
		    'is3D'   => false   
		]);

		$lava = new Lavacharts; 

		$reasons = $lava->DataTable();

		$reasons->addStringColumn('Paid')
		        ->addNumberColumn('Percent')
		        ->addRow(['Paid',  Annualamount::totalpaidincome($payment->aid) ])
		        ->addRow(['Unpaid', Annualamount::totalexpectedincome($payment->aid) - Annualamount::totalpaidincome($payment->aid)]);
		       
		 $Chart = \Lava::PieChart($annualgeneralchart, $reasons, [
		    'is3D'   => false   
		]);


		




@endphp

