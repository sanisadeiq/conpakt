@php
use Khill\Lavacharts\Lavacharts;
use App\Charges;
use App\Consumptionrate;
$lava = new Lavacharts; 
$currentchart = "currentchart".$payment->aid;
$generalchart = "generalchart".$payment->aid;

		$reasons = $lava->DataTable();

		$reasons->addStringColumn('Paid')
		        ->addNumberColumn('Percent')
		        ->addRow(['Paid',  Charges::monthpaidspecificcharge($payment->aid) ])
		        ->addRow(['Unpaid', Charges::specificcharge($payment->aid) - Charges::monthpaidspecificcharge($payment->aid)]);
		       
		 $Chart = \Lava::PieChart($currentchart, $reasons, [
		    'is3D'   => false   
		]);

		$lava = new Lavacharts; 

		$reasons = $lava->DataTable();

		$reasons->addStringColumn('Paid')
		        ->addNumberColumn('Percent')
		        ->addRow(['Paid',  Consumptionrate::specificchargetotalpaid($payment->aid) ])
		        ->addRow(['Unpaid', Consumptionrate::specificchargetotal($payment->aid) - Consumptionrate::specificchargetotalpaid($payment->aid)]);
		       
		 $Chart = \Lava::PieChart($generalchart, $reasons, [
		    'is3D'   => false   
		]);
   
@endphp

