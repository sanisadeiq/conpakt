@extends('layouts.app')

@section('content')

   
        <div class="col-md-9" style="margin: 0px;padding: 0px;overflow-y: scroll;height: 93vh">
            <div class="panel panel-default" style="margin: 0px;">
             
                <div class="panel-body" style="margin-top: 0px">
                  <h3 align="left" style="margin-top: 0px!important">Dashboard</h3>
                  <div style="margin: 0px;padding: 0px">
                  <ul class="nav nav-pills nav-justified" style="margin-top: 40px">
                      <li class="active"><a data-toggle="tab"  href="#general" >General Payment Analysis</a></li>
                        <li><a data-toggle="tab"  href="#mpayments">Monthly Payment Analysis</a></li>

                      <li><a data-toggle="tab" href="#annualpayments">Annual Payments Analysis</a></li>
                  </ul>
                <div class="tab-content">
  

                <div style="margin: 0px;padding: 0px" id="general" class="tab-pane fade in active">
                   @include('management.tabs.generaldashboard')
                </div>

                <div style="margin: 0px;padding: 0px" id="mpayments" class="tab-pane fade">
                  @include('management.tabs.monthlypayments')
                </div>

                <div style="margin: 0px;padding: 0px" id="annualpayments" class="tab-pane fade">
                  @include('management.tabs.annualpayments')
              </div>
              </div>
                </div>
                </div>
            </div>
        </div>
   

@endsection

