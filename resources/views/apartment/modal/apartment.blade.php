     <div id="apartment{{$section->id}}" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{$section->sectionname}} Add Apartment</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/addpartment') }}">
                               {{ csrf_field() }}
        <div class="form-group{{ $errors->has('apartmentnumber') ? ' has-error' : '' }}">
                 <label for="apartmentnumber" class="col-md-4 control-label">Apartment Number</label>
              

                <input type="hidden" name="sectionid" value="{{$section->id}}">
                  <div class="col-md-6">
                      <input id="apartmentnumber" type="text" class="form-control" name="apartmentnumber" value="{{ old('apartmentnumber') }}" required >
       
                         @if ($errors->has('apartmentnumber'))
                              <span class="help-block">
                                   <strong>{{ $errors->first('apartmentnumber') }}</strong>
                                    </span>
                                       @endif
                       </div>
            </div>

             <div class="form-group{{ $errors->has('squarefeet') ? ' has-error' : '' }}">
                      <label for="squarefeet" class="col-md-4 control-label">Square Feet</label>
            
                       <div class="col-md-3">
                           <input id="squarefeet" type="number" class="form-control" name="squarefeet" value="{{ old('squarefeet') }}" required >
            
                              @if ($errors->has('squarefeet'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('squarefeet') }}</strong>
                                         </span>
                                            @endif
                            </div>
                 </div>
       
       <div class="form-group">
            <div class="col-md-8 col-md-offset-4">
           <button type="submit" class="btn btn-primary">
                          Add
            </button>
       
                                      
                                   </div>
         </div>
          </form>
       
       
       
        
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>