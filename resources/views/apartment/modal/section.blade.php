     <div id="section" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Create Section</h4>
      </div>
      <div class="modal-body">
         <form class="form-horizontal" role="form" method="POST" action="{{ url('/registersection') }}">
                                {{ csrf_field() }}
         <div class="form-group{{ $errors->has('sectionname') ? ' has-error' : '' }}">
                  <label for="sectionname" class="col-md-4 control-label">Section Name</label>
        
                   <div class="col-md-6">
                       <input id="sectionname" type="text" class="form-control" name="sectionname" value="{{ old('sectionname') }}" required >
        
                          @if ($errors->has('sectionname'))
                               <span class="help-block">
                                    <strong>{{ $errors->first('sectionname') }}</strong>
                                     </span>
                                        @endif
                        </div>
             </div>
        
        <div class="form-group">
             <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                           Add
             </button>
        
                                       
                                    </div>
          </div>
           </form>
        
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>