@extends('layouts.app')

@section('content')
        <div class="col-md-9" style="margin: 0px;padding: 0px;overflow-y: scroll;height: 93vh">
            <div class="panel panel-default" style="margin: 0px;">
               

                <div class="panel-body">
                     <h3 align="left" style="margin-top: 0px!important" >Manage Apartments</h3>
                  <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#section" >Create Section</button>
                    @include('apartment.modal.section')


                    <div class="col-md-12" style="margin-top: 60px">
                        @foreach(Auth::user()->sections as $section)
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">{{$section->sectionname}}</div>
                                    <div class="panel-body">
                                        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#apartment{{$section->id}}">Add Apartment</button>
                                        @include('apartment.modal.apartment')

                                        <div class="col-md-12" style="margin-top: 20px">
                                            @if($section->apartments->count() == 0)
                                            <h3 align="center">No Apartments</h3>
                                            @else
                                           @include('apartment.table.apartmentslist')
                                            @endif
                                        </div>
                                    </div>
                                    
                                </div>
                                
                            </div>
                        @endforeach
                        
                    </div>
             
                </div>
            </div>
        </div>
    
@endsection
