@extends('layouts.app')

@section('content')

        <div class="col-md-9" style="margin: 0px;padding: 0px;overflow-y: scroll;height: 93vh">
            <div class="panel panel-default">
                <div class="panel-body">
                       <h3 align="left" style="margin-top: 0px!important;padding-left: 3px" >{{$info->apartmentnumber}}</h3>

                	<div class="col-md-10 col-md-offset-1">
                		<h3 align="center" style="margin-bottom: 30px">OWNER INFORMATION</h3>
                		
                		@if($ownercheck->count() > 0)
                		<div class="col-md-12">
                			<h3 align="center">Owner Information</h3>
                			<div class="col-md-3 pull-right">
                			<img src="{{ asset($ownercheck->photograph) }}" class="img img-responsive">
                			</div>

                			<div class="col-md-12" style="margin-top: 40px">
                				<table class="table table-hover">
                					<tr>
                						<td>
                							Name
                						</td>
                						<td>
                							{{$ownercheck->name}}
                						</td>
                					</tr>
                					<tr>
                						<td>
                							IC Number
                						</td>
                						<td>
                							{{$ownercheck->icnumber}}
                						</td>
                					</tr>
                					<tr>
                						<td>
                							Email
                						</td>
                						<td>
                							{{$ownercheck->email}}
                						</td>
                					</tr>
                					<tr>
                						<td>
                							Phone Number
                						</td>
                						<td>
                							{{$ownercheck->contactnumber}}
                						</td>
                					</tr>

                					<tr>
                						<td>
                							Contact Address
                						</td>
                						<td>
                							{{$ownercheck->contactaddress}}
                						</td>
                					</tr>


                				</table>
                			</div>
                		</div>
                		@else
                		 <form class="form-horizontal" role="form" method="POST" action="{{ url('/newowner') }}" enctype="multipart/form-data">
                		                        {{ csrf_field() }}
                		 <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                		          <label for="name" class="col-md-4 control-label">Name </label>
                			<input type="hidden" name="apartmentid" value="{{$id}}">
                		           <div class="col-md-6">
                		               <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                		
                		                  @if ($errors->has('name'))
                		                       <span class="help-block">
                		                            <strong>{{ $errors->first('name') }}</strong>
                		                             </span>
                		                                @endif
                		                </div>
                		     </div>

                		      <div class="form-group{{ $errors->has('contactnumber') ? ' has-error' : '' }}">
                		               <label for="contactnumber" class="col-md-4 control-label">Contact Number</label>
                		     
                		                <div class="col-md-6">
                		                    <input id="contactnumber" type="text" class="form-control" name="contactnumber" value="{{ old('contactnumber') }}" required >
                		     
                		                       @if ($errors->has('contactnumber'))
                		                            <span class="help-block">
                		                                 <strong>{{ $errors->first('contactnumber') }}</strong>
                		                                  </span>
                		                                     @endif
                		                     </div>
                		          </div>

                		           <div class="form-group{{ $errors->has('contactaddress') ? ' has-error' : '' }}">
                		                    <label for="contactaddress" class="col-md-4 control-label">Contact Address</label>
                		          
                		                     <div class="col-md-6">
                		                         
                		          			<textarea id="contactaddress" class="form-control" name="contactaddress" value="{{ old('contactaddress') }}" rows="5" required ></textarea>
                		                            @if ($errors->has('contactaddress'))
                		                                 <span class="help-block">
                		                                      <strong>{{ $errors->first('contactaddress') }}</strong>
                		                                       </span>
                		                                          @endif
                		                          </div>
                		               </div>	

                		                <div class="form-group{{ $errors->has('icnumber') ? ' has-error' : '' }}">
                		                         <label for="icnumber" class="col-md-4 control-label">IC Number</label>
                		               
                		                          <div class="col-md-6">
                		                              <input id="icnumber" type="text" class="form-control" name="icnumber" value="{{ old('icnumber') }}" required >
                		               
                		                                 @if ($errors->has('icnumber'))
                		                                      <span class="help-block">
                		                                           <strong>{{ $errors->first('icnumber') }}</strong>
                		                                            </span>
                		                                               @endif
                		                               </div>
                		                    </div>

                		                     <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                		                              <label for="email" class="col-md-4 control-label">Email Address</label>
                		                    
                		                               <div class="col-md-6">
                		                                   <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required >
                		                    
                		                                      @if ($errors->has('email'))
                		                                           <span class="help-block">
                		                                                <strong>{{ $errors->first('email') }}</strong>
                		                                                 </span>
                		                                                    @endif
                		                                    </div>
                		                         </div>

                		                          <div class="form-group{{ $errors->has('photograph') ? ' has-error' : '' }}">
                		                                   <label for="photograph" class="col-md-4 control-label">Photograph</label>
                		                         
                		                                    <div class="col-md-6">
                		                                        <input id="photograph" type="file" class="form-control" name="photograph" value="{{ old('photograph') }}" required >
                		                         
                		                                           @if ($errors->has('photograph'))
                		                                                <span class="help-block">
                		                                                     <strong>{{ $errors->first('photograph') }}</strong>
                		                                                      </span>
                		                                                         @endif
                		                                         </div>
                		                              </div>
                		
                		<div class="form-group">
                		     <div class="col-md-8 col-md-offset-4">
                		    <button type="submit" class="btn btn-primary">
                		                   Save
                		     </button>
                		
                		                               
                		                            </div>
                		  </div>
                		   </form>
                		
                		@endif

                		
                		
                	</div>

                	<div class="col-md-12" style="margin-top: 20px">
                		<h3 align="center">MONTH CHARGES PAYMENT HISTORY</h3>
                		<table class="table table-hover table-bordered" style="margin-top: 40px;text-align: center" align="center">
                            <thead>
                                <th style="text-align: center">
                                    #
                                </th>
                                <th style="text-align: center">
                                    Charge Name
                                </th>
                                <th style="text-align: center">
                                    Charge Period
                                </th>
                                <th style="text-align: center">
                                    Amount Due
                                </th>
                            </thead>
                            <tbody>
                                @foreach($charges as $charge)
                                    <tr>
                                        <td>
                                            {{$loop->index + 1}}
                                        </td>
                                        <td>
                                            {{$charge->chargetype}}
                                        </td>
                                        <td>
                                            {{$charge->created_at->subMonth()->format('F Y')}}
                                        </td>
                                        <td>
                                            @if($charge->status == 0)
                                            <span data-toggle="modal" data-target="#myModal{{$charge->mid}}">
                                               {!! App\Helpers\amounts::calculateamount($id, $charge) !!}
                                            </span>
                                            @else
                                            {!! App\Helpers\amounts::calculateamount($id, $charge) !!}
                                            @endif
                                           
                                        </td>
                                    </tr>
                                 @include('payment.modal.payment')
                                @endforeach
                            </tbody>
                            
                        </table>
                	</div>

                    <div class="col-md-12" style="margin-top: 20px">
                        <h3 align="center">ANNUAL CHARGES PAYMENT HISTORY</h3>
                        <table class="table table-hover table-bordered" style="margin-top: 40px;text-align: center" align="center">
                            <thead>
                                <th style="text-align: center">
                                    #
                                </th>
                                <th style="text-align: center">
                                    Charge Name
                                </th>
                                <th style="text-align: center">
                                    Charge Period
                                </th>
                                <th style="text-align: center">
                                    Amount Due
                                </th>
                            </thead>
                            <tbody>
                                @foreach($annualcharges as $charge)
                                    <tr>
                                        <td>
                                            {{$loop->index + 1}}
                                        </td>
                                        <td>
                                            {{$charge->chargetype}}
                                        </td>
                                        <td>
                                            {{$charge->created_at->subYear()->format('Y')}}
                                        </td>
                                        <td>
                                            @if($charge->status == 0)
                                            <span data-toggle="modal" data-target="#annualpayment{{$charge->mid}}">
                                               {!! App\Helpers\annualamounts::calculateamount($id, $charge) !!}
                                            </span>
                                            @else
                                            {!! App\Helpers\annualamounts::calculateamount($id, $charge) !!}
                                            @endif
                                           
                                        </td>
                                    </tr>
                                 @include('payment.modal.annualpayment')
                                @endforeach
                            </tbody>
                            
                        </table>
                    </div>
                
                </div>
            </div>
        </div>
   
@endsection
