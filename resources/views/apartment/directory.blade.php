@extends('layouts.app')

@section('content')
        <div class="col-md-9" style="margin: 0px;padding: 0px;overflow-y: scroll;height: 93vh">
            <div class="panel panel-default" style="margin: 0px;">
               

                <div class="panel-body">
                     <h3 align="left" style="margin-top: 0px!important" >Owners Directory</h3>
                
                <div class="col-md-12" style="margin-top: 50px">
                    <table class="table table-hover table-bordered" >
                    <thead>
                        <th>
                            #
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Contact Number
                        </th>
                        <th>
                            Email Address
                        </th>
                        <th>
                            Apartment(s) Owned
                        </th>
                    </thead>
                    <tbody>
                        @foreach($owners as $owner)
                            <tr>
                                <td>
                                    {{$loop->index + 1}}
                                </td>
                                <td>
                                    {{$owner->name}}
                                </td>
                                <td>
                                    {{$owner->contactnumber}}
                                </td>
                                <td>
                                    {{$owner->email}}
                                </td>
                                <td>
                                    @foreach(App\Apartment_owner_pivot::apartmentsowned($owner->id) as $apartment)
                                    <a href="{{ url('/apartment', [$apartment->id]) }}">
                                        {{$apartment->apartmentnumber}} 
                                    </a><br/>
                                    @endforeach
                                </td>
                            </tr>
                        @endforeach
                        
                    </tbody>
                    
                </table>
                </div>
             
                </div>
            </div>
        </div>
    
@endsection
