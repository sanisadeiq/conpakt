  <table class="table table-hover">
                                                <thead>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        Apartment Number
                                                    </th>
                                                    <th>
                                                        Square Feet
                                                    </th>
                                                </thead>
                                                 <tbody>

                                            @foreach($section->apartments as $apartment)
                                            <tr>
                                                <td>
                                                    {{$loop->index + 1}}
                                                </td>
                                                <td>
                                                    <a href="{{ url('/apartment', [$apartment->id]) }}">{{$apartment->apartmentnumber}}</a>
                                                </td>
                                                <td>
                                                    {{$apartment->squarefeet}}
                                                </td>
                                            </tr>
                                               
                             @endforeach
                                                </tbody>
                                                
                                            
                                           
                                            </table>