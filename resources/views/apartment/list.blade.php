@extends('layouts.app')

@section('content')
        <div class="col-md-9" style="margin: 0px;padding: 0px;overflow-y: scroll;height: 93vh">
            <div class="panel panel-default" style="margin: 0px;">
               

                <div class="panel-body">
                     <h3 align="left" style="margin-top: 0px!important" >Apartments List</h3>
                
                <div class="col-md-12" style="margin-top: 50px">
                    <table class="table table-hover table-bordered" >
                    <thead>
                        <th>
                            #
                        </th>
                        <th>
                            Apartment Number
                        </th>
                        <th>
                            Square Feet
                        </th>
                        <th>
                            Section
                        </th>
                    </thead>
                    <tbody>
                        @foreach(Auth::user()->condo as $apartment)
                            <tr>
                                <td>
                                    {{$loop->index + 1}}
                                </td>
                                <td>
                                    {{$apartment->apartmentnumber}}
                                </td>
                                <td>
                                    {{$apartment->squarefeet}}
                                </td>
                                <td>
                                    {{ App\Section::sectionname($apartment->sectionid) }}
                                   
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    
                </table>
                </div>
             
                </div>
            </div>
        </div>
    
@endsection
