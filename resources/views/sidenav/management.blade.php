       <div class="col-md-3 navbar-inverse" align="left" style="margin-top: 0px;background-color: black;opacity: 0.8; height: 93vh;padding: 0px;margin-right: 0px">

<ul class="nav nav-pills nav-stacked " style="margin-top: 40px;font-size: 15px">
  <li style="margin:0px;padding: 0px"> <a href="{{ url('/home')}}" >

  <span class="fa fa-tachometer" aria-hidden="true" style="padding-right: 10px"></span>Dashboard
  </a></li>
  <li> <a href="#demo" data-toggle="collapse">
  <span class="fa fa-building" aria-hidden="true" style="padding-right: 10px"></span>

  Apartments<span class="fa fa-chevron-down pull-right" aria-hidden="true" style="font-size: 12px"></span>
  </a>
  <ul id="demo" class="collapse nav nav-pills nav-stacked">

       <li style="margin-top: 4px;margin-left: 4px">

        <a href="{{ url('/apartments-management')}}">
           <span class="fa fa-tasks" aria-hidden="true"></span> Manage Apartments</a> </li>
     <li style="margin-left: 4px"> <a href="{{ url('/apartments-list')}}">
      <span class="fa fa-list-alt" aria-hidden="true"></span>  Apartments List</a> </li>
     <li style="margin-left: 4px">

      <a href="{{ url('/owners-directory')}}"> 
        <span class="fa fa-address-book-o" aria-hidden="true"></span> Owners Directory </a> </li>
  </ul>

   </li>
<li > <a href="{{ url('/charges')}}" >

  <span class="fa fa-money" aria-hidden="true" style="padding-right: 10px"></span>Charges
  </a></li>



    <li> <a href="#payments" data-toggle="collapse">
  <span class="fa fa-code-fork" aria-hidden="true" style="padding-right: 10px"></span>
Payments<span class="fa fa-chevron-down pull-right" aria-hidden="true" style="padding-left: 30px;font-size: 12px"></span>
  </a>
  <ul id="payments" class="collapse nav nav-pills nav-stacked">

    <li style="margin-top: 4px;margin-left: 4px"> <a href="#monthlypayments" data-toggle="collapse">
    <span class="fa fa-calendar" aria-hidden="true" style="padding-right: 10px"></span>Monthly Payments <span class="fa fa-chevron-down pull-right" aria-hidden="true" style="font-size: 12px"></span>
     </a>
      <ul id="monthlypayments" class="collapse nav nav-pills nav-stacked">
        <li style="margin-left: 6px">
        <a href="{{ url('/monthly-payments')}}" style="margin-top: 4px">
    <span class="fa fa-hourglass-half" aria-hidden="true" style="padding-right: 10px"></span> Payment Status </a>
      </li>
        <li style="margin-left: 6px">
        <a href="{{ url('/monthly-consumption')}}">
    <span class="fa fa-pencil-square-o" aria-hidden="true" style="padding-right: 10px"></span> Enter Consumption Rate </a>
      </li>
      </ul>
     </li>


     <li style="margin-left: 4px"> <a href="#annualpayment" data-toggle="collapse">
    <span class="fa fa-calendar" aria-hidden="true" style="padding-right: 10px"></span>Annual Payments <span class="fa fa-chevron-down pull-right" aria-hidden="true" style="font-size: 12px"></span>
     </a>
      <ul id="annualpayment" class="collapse nav nav-pills nav-stacked">
        <li style="margin-left: 6px">
        <a href="{{ url('/annual-payments')}}" style="margin-top: 4px">
    <span class="fa fa-hourglass-half" aria-hidden="true" style="padding-right: 10px"></span> Payment Status </a>
      </li>
        <li style="margin-left: 6px">
        <a href="{{ url('/annual-consumption')}}">
    <span class="fa fa-pencil-square-o" aria-hidden="true" style="padding-right: 10px"></span> Enter Consumption Rate </a>
      </li>
      </ul>
     </li>
  </ul>

   </li>

   <li> <a href="#reports" data-toggle="collapse">
  <span class="fa fa-line-chart" aria-hidden="true" style="padding-right: 10px"></span>
Reports<span class="fa fa-chevron-down pull-right" aria-hidden="true" style="padding-left: 30px;font-size: 12px"></span>
  </a>
  <ul id="reports" class="collapse nav nav-pills nav-stacked">
    
    <li style="margin-top: 4px;margin-left: 4px"> <a href="{{ url('/monthly-charges-report')}}">
    <span class="fa fa-line-chart" aria-hidden="true" style="padding-right: 10px"></span>Monthly Charges Report </a> </li>


    <li style="margin-left: 4px"> <a href="{{ url('/annual-charges-report')}}">
    <span class="fa fa-line-chart" aria-hidden="true" style="padding-right: 10px"></span> Annual Payments </a> </li>
  </ul>

   </li>

    <li> <a href="#announcements" data-toggle="collapse">
  <span class="fa fa-bullhorn" aria-hidden="true" style="padding-right: 10px"></span>
Announcement<span class="fa fa-chevron-down pull-right" aria-hidden="true" style="padding-left: 30px;font-size: 12px"></span>
  </a>
  <ul id="announcements" class="collapse nav nav-pills nav-stacked">
     <li><a href="{{ url('/sms')}}" style="margin-top: 4px">
      SMS
    </a> </li>

    <li> <a href="{{ url('/email')}}"> Email </a> </li>





  </ul>

   </li>




</ul>

</div>