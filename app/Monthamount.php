<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;

use Carbon\Carbon;

class Monthamount extends Model
{
    

    protected $fillable = [
    	'chargeid',
    	'amount',
    ];

    public static function today()
    {
        return Carbon::today();
    }

    public static function currentmonthcharges($today = ' ')
    {
    	$today = Carbon::today();

        if($today == ' ')
        {
            $today = Carbon::today();
        }

    	return Monthamount::join('charges', 'charges.id', 'monthamounts.chargeid')
    						->where('charges.condoid', Auth::user()->id)
    						->whereMonth('monthamounts.created_at', $today->month)
    						->select('charges.id', 'charges.chargetype', 'monthamounts.created_at')
    						->get();
    }
}
