<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

use Carbon\Carbon;

class Consumptionrate extends Model
{
    protected $fillable = [
    	'apartmentid',
    	'chargeid',
    	'amountconsumed',
    ];

    public function Apartment()
    {
    	$this->belongsTo('App\Apartmentid');
    }

    public static function checkconsumptionrate($apartmentid, $chargeid)
    {
        $today = Carbon::today();

    	return Consumptionrate::where('apartmentid', $apartmentid)
    							->where('chargeid', $chargeid)
                                ->whereMonth('consumptionrates.created_at', $today->month)
    							->join('charges', 'charges.id', 'consumptionrates.chargeid')
    							->select('charges.amount as chargeamount', 'consumptionrates.amountconsumed')
    							->select(DB::raw('charges.amount * consumptionrates.amountconsumed as consumptioncost'))
    							->first();
    }

    public static function specificchargetotal($chargeid)
    {
        $months = Monthamount::where('chargeid', $chargeid)->get();

        $sumtotal = array();

        foreach($months as $month)
        {
           $amountconsumed = Consumptionrate::where('chargeid', $chargeid)
                           ->whereMonth('created_at', $month->created_at->month)
                           ->sum('amountconsumed');

            $monthtotal = $month->amount * $amountconsumed;

            $sumtotal[] = $monthtotal;
        }

        return array_sum($sumtotal);
    }

    public static function specificchargetotalpaid($chargeid)
    {
         $months = Monthamount::where('chargeid', $chargeid)->get();
        // dd($months);
        $sumtotal = array();

        foreach($months as $month)
        {
           $getapartments = Monthpayment::where('chargeid', $chargeid)
                                         ->where('status', 1)
                                         ->whereMonth('created_at', $month->created_at->month)
                                         ->select('apartmentid as aid')
                                         ->get();
                                       
            $amountconsumed = Consumptionrate::where('chargeid', $chargeid)
                                            ->whereMonth('created_at', $month->created_at->month)
                                            ->whereIN('apartmentid', $getapartments->pluck('aid'))
                                            ->sum('amountconsumed');
                                           
            $monthtotal = $month->amount * $amountconsumed;

            $sumtotal[] = $monthtotal;
        }

         return array_sum($sumtotal);

    }

}
