<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apartment_owner_pivot extends Model
{
    protected $fillable = [
    	'apartmentid',
    	'ownerid',
    ];

    public static function apartmentsowned($ownerid)
    {
    	return  Apartment_owner_pivot::where('ownerid', $ownerid)
									   ->join('apartments', 'apartments.id', 'apartment_owner_pivots.apartmentid')
									   ->select('apartments.id', 'apartments.apartmentnumber')
									   ->get();

    }

    public function scopeOwnercheck($query, $id)
    {
        return $query->where('apartmentid', $id)
                ->join('users', 'users.id', 'apartment_owner_pivots.ownerid')
                ->join('owners', 'owners.loginid', 'users.id')
                ->select('owners.*', 'users.*')
                ->first();
    }
}
