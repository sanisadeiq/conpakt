<?php

namespace App;

use Auth;

use Carbon\Carbon;


use Illuminate\Database\Eloquent\Model;

class Charges extends Model
{
    protected $fillable = [
    	'condoid',
    	'chargetype',
    	'amount',
    	'billingtype',
        'chargingtype'
    ];

    public function User()
    {
    	return $this->belongsTo('App\User');
    }

    public function getbillingtypeAttribute($value) {
    	if($value == 1)
    	{
    		return 'Monthly';
    	}elseif($value == 2)
    	{
    		return 'Annually';
    	}
    }

    public function getchargingtypeAttribute($value)
    {
        if($value == 1)
        {
            return 'Fixed';
        }elseif($value == 2)
        {
            return 'Per Consumption';
        }
    }

     public function setchargetypeAttribute($value)
    {
        $this->attributes['chargetype'] = title_case($value);
    }

    
    public function scopeMonthlyTransactions($query)
    {
        return $query->where('billingtype', 1);
    }

    public function scopeAnnualTransactions($query)
    {
        return $query->where('billingtype', 2);
    }

    public function scopeServicecharge($query)
    {
        return $query->where('charges.condoid', Auth::user()->id)
                    ->where('charges.chargetype', 'Service Charge');
    }

    public static function specificcharge($id)
    {
        $today = Carbon::today();

        $othercharges = Consumptionrate::where('chargeid', $id)
                                        ->whereMonth('created_at', $today->month)
                                        ->sum('amountconsumed');
                                        
        $monthrate = Monthamount::where('chargeid', $id)->whereMonth('created_at', $today->month)->first();

       // dd($monthrate);


        return $othercharges * $monthrate->amount; 

    }

    public static function monthpaidspecificcharge($id)
    {
                $today = Carbon::today();

                $paidapartments = Monthpayment::where('chargeid', $id)
                                    ->where('status', 1)
                                    ->whereMonth('created_at', $today->month)
                                    ->select('apartmentid')
                                    ->get();
                

                $quantity = Consumptionrate::where('chargeid', $id)
                                           ->whereIn('apartmentid', $paidapartments->pluck('apartmentid'))
                                            ->whereMonth('created_at', $today->month)
                                            ->sum('amountconsumed');
                
                                            
                $monthrate = Monthamount::where('chargeid', $id)->first();

                 return  $quantity * $monthrate->amount;

    }
}
