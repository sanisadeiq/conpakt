<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\status;


class Annualpayment extends Model
{
	use status;
	
   protected $fillable = [
    	'chargeid',
    	'apartmentid',
    	'status',
    ];

    public function getstatusAttribute($value) {
    	
        return $this->checkpaymentstatus($value);
    }
    public function scopeapartmentchargerecord($query, $id)
    {
        return $query->where('apartmentid', $id)
                    ->join('charges', 'charges.id', 'annualpayments.chargeid')
                    ->select('charges.id as id', 'charges.*', 'annualpayments.created_at', 'charges.billingtype', 'annualpayments.status', 'annualpayments.id as mid', 'annualpayments.created_at')
                    ->get();
    }
}
