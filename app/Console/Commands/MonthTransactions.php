<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Charges;

use App\Monthamount;

use App\Monthpayment;

use Carbon\Carbon;

class MonthTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transactions:monthly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Monthly Transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         ini_set('memory_limit', '-1');
         ini_set('max_execution_time', 0);
            $this->info($this->timestamp() . "Processing Monthly Transactions.");
            $this->getmonthlytransactions();
            $this->getapartmentswithmonthlytransactions();
            $this->info($this->timestamp() . "Finshed Processing Monthly Transactions.");
        ini_set('memory_limit', '-1');

        return true;
    }

    protected function getmonthlytransactions()
    {
        $charges = Charges::MonthlyTransactions()->get();

        foreach($charges as $charge)
        {
            $amounts = New Monthamount();
            $amounts->chargeid = $charge->id;
            $amounts->amount = $charge->amount;
            $amounts->save();
        }

    }

    protected function getapartmentswithmonthlytransactions()
    {
        $apartments = Charges::MonthlyTransactions()
                            ->join('condominia', 'condominia.loginid', 'charges.condoid')
                            ->join('apartments', 'apartments.condoid', 'condominia.loginid')
                            ->select('apartments.id as aid', 'charges.id as cid')
                            ->get();

         foreach($apartments as $apartment)
        {
            $payment = New Monthpayment();
            $payment->apartmentid = $apartment->aid;
            $payment->chargeid = $apartment->cid;
            $payment->save();

        }
    }

    protected function timestamp()
    {
        $timestamp = date("Y-m-d H:i:s");
        return "[$timestamp] ";
    }
}
