<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Annualamount;

use App\Annualpayment;

use App\Charges;

use Carbon\Carbon;

class AnnualTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transactions:annualy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Annual Transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         ini_set('memory_limit', '-1');
         ini_set('max_execution_time', 0);
            $this->info($this->timestamp() . "Processing Annual Transactions.");
            $this->getannualtransactions();
            $this->getapartmentswithannualtransactions();
            $this->info($this->timestamp() . "Finshed Processing Annual Transactions.");
        ini_set('memory_limit', '-1');

        return true;
    }

    protected function getannualtransactions()
    {
        $charges = Charges::AnnualTransactions()->get();

        foreach($charges as $charge)
        {
            $amounts = New Annualamount();
            $amounts->chargeid = $charge->id;
            $amounts->amount = $charge->amount;
            $amounts->save();
        }

    }

    protected function getapartmentswithannualtransactions()
    {
        $apartments = Charges::AnnualTransactions()
                            ->join('condominia', 'condominia.loginid', 'charges.condoid')
                            ->join('apartments', 'apartments.condoid', 'condominia.loginid')
                            ->select('apartments.id as aid', 'charges.id as cid')
                            ->get();

         foreach($apartments as $apartment)
        {
            $payment = New Annualpayment();
            $payment->apartmentid = $apartment->aid;
            $payment->chargeid = $apartment->cid;
            $payment->save();

        }
    }

    protected function timestamp()
    {
        $timestamp = date("Y-m-d H:i:s");
        return "[$timestamp] ";
    }
}