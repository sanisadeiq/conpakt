<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'usertype'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'usertype'
    ];

    public function sections()
    {
        return $this->hasMany('App\Section', 'condoid');
    }

    public function charges()
    {
        return $this->hasMany('App\Charges', 'condoid');
    }

    public function condo()
    {
        return $this->hasMany('App\Apartment', 'condoid');
    }

     public function setNameAttribute($value)
    {
        $this->attributes['name'] = title_case($value);
    }

    public function management()
    {
        return $this->hasOne('App\Condominium', 'loginid');
    }
}
