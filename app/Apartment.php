<?php

namespace App;

use Auth;

use Carbon\Carbon;

use DB;

use Illuminate\Database\Eloquent\Model;

class Apartment extends Model
{
    protected $fillable = [
    	'sectionid',
    	'apartmentnumber',
    	'squarefeet',
    	'condoid'
    ];

    public function section()
    {
    	return $this->belongsTo('App\Section');
    }

    public function Owner()
    {
        return $this->belongsTo('App\User');
    }

    public function Consumptionrate()
    {
        $this->hasMany('App\Consumtionrate');
    }


    //for situations when you dont need the payment status to be mutated
    public static function paymentstatus($chargeid, $monthlypaymentid)
    {
        return  Charges::join('monthpayments', 'monthpayments.chargeid', '=', 'charges.id')
                        ->where('monthpayments.id', $monthlypaymentid)
                        ->where('monthpayments.chargeid', $chargeid)
                        ->select('monthpayments.status')
                        ->first();
    }

    public static function monthlypaymentlist()
    {
        $today = Carbon::today();

        return Apartment::where('condoid', Auth::user()->id)
                        ->join('monthpayments', 'monthpayments.apartmentid', 'apartments.id')
                        ->whereMonth('monthpayments.created_at', $today->month)
                        ->select('apartments.id as aid', 'apartments.apartmentnumber', 'monthpayments.status')
                        ->get();
    }

    public static function overallpaymentlist()
    {
        return Apartment::where('condoid', Auth::user()->id)
                        ->join('monthpayments', 'monthpayments.apartmentid', 'apartments.id')
                        ->select('apartments.id as aid', 'apartments.apartmentnumber', 'monthpayments.status', 'monthpayments.created_at')
                        ->get();
    }

    public function scopeCondoApartments($query)
    {
        return $query->where('condoid', Auth::user()->id);
    }

    public static function annualchargesdefaulterslist()
    {
        
        return Apartment::where('apartments.condoid', Auth::user()->id)
                        ->join('annualpayments', 'annualpayments.apartmentid', 'apartments.id')
                        ->where('annualpayments.status', 0)
                        ->join('charges', 'charges.id', 'annualpayments.chargeid')
                        ->select('apartments.id as aid', 'apartments.apartmentnumber', 'annualpayments.status', 'annualpayments.created_at', 'charges.id as cid', 'charges.chargetype', 'charges.chargingtype')
                        ->get();
    }

    public static function annualchargescurrentyeardefaulterslist()
    {
        $today = Carbon::today();


        return Apartment::where('condoid', Auth::user()->id)
                        ->join('annualpayments', 'annualpayments.apartmentid', 'apartments.id')
                        ->where('annualpayments.status', 0)
                        ->whereYear('annualpayments.created_at', $today->year)
                        ->select('apartments.id as aid', 'apartments.apartmentnumber', 'annualpayments.status', 'annualpayments.created_at')
                        ->get();
    }


     public static function annualchargedefaulterslist($chargeid)
    {
        
        return Apartment::where('apartments.condoid', Auth::user()->id)
                        ->join('annualpayments', 'annualpayments.apartmentid', 'apartments.id')
                        ->where('annualpayments.status', 0)
                        ->join('charges', 'charges.id', 'annualpayments.chargeid')
                        ->where('charges.id', $chargeid)
                        ->select('apartments.id as aid', 'apartments.apartmentnumber', 'annualpayments.status', 'annualpayments.created_at', 'charges.id as cid', 'charges.chargetype', 'charges.chargingtype')
                        ->get();
    }

    
}
