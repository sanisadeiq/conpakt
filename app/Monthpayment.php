<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

use Auth;

use App\Helpers\status;

class Monthpayment extends Model
{
    use status;
    protected $fillable = [
    	'chargeid',
    	'apartmentid',
    	'status',
    ];


    public function getstatusAttribute($value) {
    	
        return $this->checkpaymentstatus($value);
    }

    public function scopeapartmentchargerecord($query, $id)
    {
        return $query->where('apartmentid', $id)
                    ->join('charges', 'charges.id', 'monthpayments.chargeid')
                    ->select('charges.id as id', 'charges.*', 'monthpayments.created_at', 'charges.billingtype', 'monthpayments.status', 'monthpayments.id as mid')
                    ->get();
    }

    public static function paymentstatus($apartmentid, $chargeid)
    {
        $today = Carbon::today();
        
        return  Monthpayment::where('apartmentid', $apartmentid)
                            ->where('chargeid', $chargeid)
                            ->whereMonth('created_at', $today->month)
                            ->first();
    }



    public static function currentmonthtotalservicecharge()
    {
                $today = Carbon::today();

        $totalsquarefeet = Apartment::where('condoid', Auth::user()->id)
                                        ->sum('squarefeet');
        $monthrate= Charges::where('chargetype', 'Service Charge')
                            ->where('condoid', Auth::user()->id)
                            ->join('monthamounts', 'monthamounts.chargeid', 'charges.id')
                            ->whereMonth('monthamounts.created_at', $today->month)
                            ->select('monthamounts.amount')
                            ->first();

        return $totalsquarefeet * $monthrate->amount;
    }

    public static function totalservicecharge()
    {
                $today = Carbon::today();

        $totalsquarefeet = Apartment::where('condoid', Auth::user()->id)
                                        ->sum('squarefeet');
        $monthrate= Charges::where('chargetype', 'Service Charge')
                            ->join('monthamounts', 'monthamounts.chargeid', 'charges.id')
                            ->select('monthamounts.amount')
                            ->first();

        return $totalsquarefeet * $monthrate->amount;
    }


    public static function allmonthsservicecharge()
    {
        $monthamounts = Charges::where('charges.condoid', Auth::user()->id)
                                ->where('charges.chargetype', 'Service Charge')
                                ->join('monthamounts', 'monthamounts.chargeid', 'charges.id')
                                ->get();
        $squarefeet = Apartment::CondoApartments()->sum('squarefeet');

        $totalamount = array();

        foreach($monthamounts as $month)
        {
            $monthtotal = $month->amount * $squarefeet;

            $totalamount[] = $monthtotal;
        }

        return array_sum($totalamount);
    }

    public static function otherchargescurrentmonthtotal()
    {
        $today = Carbon::now();

        $charges = Monthamount::join('charges', 'charges.id', 'monthamounts.chargeid')
                                ->where('charges.chargetype', '!=', 'Service Charge')
                                ->where('charges.condoid', Auth::user()->id)
                                ->whereMonth('monthamounts.created_at', $today->month)
                                ->select('charges.id as cid')
                                ->get();
        $total = array();

        foreach($charges as $charge)
        {
            $quantity = Consumptionrate::where('chargeid', $charge->cid)
                                        ->whereMonth('created_at', $today->month)
                                        ->sum('amountconsumed');

            $monthrate = Monthamount::where('chargeid', $charge->cid)->first();

            $amount = $quantity * $monthrate->amount;

             $total[] = $amount;

        }

        return array_sum($total);
    }


    public static function defaultmonthscount($chargeid, $apartmentid)
    {
        return Monthpayment::where('chargeid', $chargeid)
                        ->where('apartmentid', $apartmentid)
                        ->where('status', 0)
                        ->count();
    }

    public static function servicechargeamountdefault($apartmentid, $squarefeet)
    {
        $servicecharge = Charges::where('condoid', Auth::user()->id)
                                ->where('chargetype', 'Service Charge')
                                ->first();

       //dd($servicecharge->id);

        $payments = Monthpayment::where('chargeid', $servicecharge->id)
                                ->where('apartmentid', $apartmentid)
                                ->where('status', 0)
                                ->select('monthpayments.created_at', 'chargeid')
                                ->get();

        $total =  array();

         foreach($payments as $pay)
         {
            $amount = Monthamount::where('chargeid', $pay->chargeid)
                                ->whereMonth('created_at', $pay->created_at->month)
                                ->select('amount')
                                ->first();

            $servicecharge = $amount->amount * $squarefeet;

            $total[] = $servicecharge;

         }

        return array_sum($total);
    }

    public static function defaultmonthcount($apartmentid, $chargeid)
    {
      return  Monthpayment::where('chargeid', $chargeid)
                            ->where('apartmentid', $apartmentid)
                            ->where('status', 0)
                            ->count();
    }

    public static function defaultamount($apartmentid, $chargeid)
    {
      $defaultmonths = Monthpayment::where('apartmentid', $apartmentid)
                                    ->where('chargeid', $chargeid)
                                    ->where('status', 0)
                                    ->get();

        $totalamount = array();

        foreach($defaultmonths as $month)
        {
          $amountconsumed = Consumptionrate::where('chargeid', $chargeid)
                                      ->where('apartmentid', $apartmentid)
                                      ->whereMonth('created_at', $month->created_at->month)
                                      ->first();

            $rate = Monthamount::where('chargeid', $chargeid)
                                ->whereMonth('created_at', $month->created_at->month)
                                ->first();
           
                $cost = $amountconsumed->amountconsumed * $rate->amount;

                $totalamount[] = $cost;
          
        }
        return array_sum($totalamount);
    }

   
}

