<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = [
    	'sectionname',
    	'condoid'
    ];

    public function User()
    {
    	return $this->belongsTo('App\User');
    }

    public function Apartments()
    {
    	return $this->hasMany('App\Apartment', 'sectionid');
    }

    public static function sectionname($sectionid)
    {
        $section = Section::where('id', $sectionid)->first();

        return $section->sectionname;
    }
}
