<?php
namespace App\Helpers;

use App\Apartment;

use App\Charges;

use App\Consumptionrate;

use App\Monthamount;

use App\Monthpayment;

use DB;

use Carbon\Carbon;

use Auth;

trait monthtotal{

        

    public function paidservicecharge()
    {
      $today = Carbon::today();
      $apartments =  Monthpayment::where('status', 1)
                    ->whereMonth('monthpayments.created_at', $today->month)
                    ->join('charges', 'charges.id', 'monthpayments.chargeid')
                    ->where('charges.chargetype', '=', 'Service Charge')
                    ->join('apartments', 'apartments.id', 'monthpayments.apartmentid')
                    ->select('apartments.squarefeet')
                    ->get();

                    
        $chargeamount = Monthamount::whereMonth('monthamounts.created_at', $today->month)
                                    ->join('charges', 'charges.id', 'monthamounts.chargeid')
                                    ->where('charges.chargetype', '=', 'Service Charge')
                                    ->select('monthamounts.amount')
                                    ->first();
                                 
            return $apartments->sum('squarefeet') * $chargeamount->amount;

                
    }

    public function totalpaidservicecharge()
    {
      $today = Carbon::today();
      $apartments =  Monthpayment::where('status', 1)
                    ->whereMonth('monthpayments.created_at', $today->month)
                    ->join('charges', 'charges.id', 'monthpayments.chargeid')
                    ->where('charges.chargetype', '=', 'Service Charge')
                    ->join('apartments', 'apartments.id', 'monthpayments.apartmentid')
                    ->select('apartments.squarefeet')
                    ->get();

                    
        $chargeamount = Monthamount::whereMonth('monthamounts.created_at', $today->month)
                                    ->join('charges', 'charges.id', 'monthamounts.chargeid')
                                    ->where('charges.chargetype', '=', 'Service Charge')
                                    ->select('monthamounts.amount')
                                    ->first();
                                 
            return $apartments->sum('squarefeet') * $chargeamount->amount;

                
    }

  public function otherchargesmonthtotal()
  {
        $today = Carbon::today();

        $charges = Monthpayment::where('status', 1)
                                   ->join('charges', 'charges.id', 'monthpayments.chargeid')
                                   ->where('charges.chargetype', '!=', 'Service Charge')
                                   ->whereMonth('monthpayments.created_at', $today->month)
                                    ->select('charges.id as cid', 'monthpayments.apartmentid')
                                   ->get();
                    //dd($charges);
         $total = array();

        foreach($charges as $charge)
        {
            $quantity = Consumptionrate::where('chargeid', $charge->cid)
                                        ->whereMonth('created_at', $today->month)
                                        ->where('apartmentid', $charge->apartmentid)
                                        ->first();
            $monthrate = Monthamount::where('chargeid', $charge->cid)
                                      ->whereMonth('created_at', $today->month)
                                     ->first();

            $amount = $quantity->amountconsumed * $monthrate->amount;

             $total[] = $amount;

        }

        return array_sum($total);                   
  } 

  public function totalcollection()
  {
    return $this->paidservicecharge() + $this->otherchargesmonthtotal();
  } 

  public function statuscount($status)
  {
    $today = Carbon::today();
   return  Apartment::where('condoid', Auth::user()->id)
                  ->join('monthpayments', 'monthpayments.apartmentid', 'apartments.id')
                  ->whereMonth('monthpayments.created_at', $today->month)
                  ->where('monthpayments.status', $status)
                  ->select('apartments.id as aid')
                  ->get()->unique('aid')->count();
  }

  public function servicechargebase()
  {
       $charge = Charges::where('condoid', Auth::user()->id)
                      ->where('chargetype', 'Service Charge')
                      ->first();

    return Monthamount::where('chargeid', $charge->id)->get();
  }

  public function servicechargeexpectedincome()
  {
    $months = $this->servicechargebase();

    $expectedincome = array();

    foreach($months as $month)
    {
      $totalsquarefeet = Apartment::where('condoid', Auth::user()->id)
                                  ->sum('squarefeet');


      $monthtotal = $totalsquarefeet * $month->amount;

      $expectedincome[] = $monthtotal;
    }

      return array_sum($expectedincome);
  }

  public function totalservicechargepaid()
  {
    $months = $this->servicechargebase();

    $expectedincome = array();

    foreach($months as $month)
    {
      $totalsquarefeet = Apartment::where('apartments.condoid', Auth::user()->id)
                                  ->join('monthpayments', 'monthpayments.apartmentid', 'apartments.id')
                                  ->where('monthpayments.chargeid', $month->chargeid)
                                  ->where('monthpayments.status', 1)
                                  ->sum('apartments.squarefeet');


      $monthtotal = $totalsquarefeet * $month->amount;

      $expectedincome[] = $monthtotal;
    }

      return array_sum($expectedincome);
  }


}