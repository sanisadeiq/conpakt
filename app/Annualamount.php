<?php

namespace App;

use Carbon\Carbon;

use Auth;

use Illuminate\Database\Eloquent\Model;

class Annualamount extends Model
{
    
    protected $fillable = [
    	'chargeid',
    	'amount',
    ];

    public static function currentyearcharges($today = ' ')
    {
    	$today = Carbon::today();

        if($today == ' ')
        {
            $today = Carbon::today();
        }

    	return Annualamount::join('charges', 'charges.id', 'annualamounts.chargeid')
    						->where('charges.condoid', Auth::user()->id)
    						->whereYear('annualamounts.created_at', $today->year)
    						->select('charges.id as cid', 'charges.chargetype', 'annualamounts.created_at', 'charges.chargingtype', 'annualamounts.id')
                ->groupBy('charges.id', 'charges.chargetype', 'annualamounts.created_at', 'charges.chargingtype', 'annualamounts.id')
    						->get();
    }

    public static function currentyearexpectedincome()
    {
        $today = Carbon::today();

        $amounts = array();

        $charges = Annualamount::whereYear('annualamounts.created_at', $today->year)
                              ->join('charges', 'charges.id', 'annualamounts.chargeid')
                              ->where('charges.condoid', Auth::user()->id)
                                ->select('charges.chargingtype', 'annualamounts.amount', 'charges.id as cid')                              
                                ->get();

          foreach($charges as $charge)
          {
                if($charge->chargingtype == 1)
                {
                  $amount = $charge->amount * Apartment::CondoApartments()->count();

                  $amounts[] = $amount;
                }
                else{

                    $totalconsumed = Consumptionrate::where('chargeid', $charge->cid)
                                                    ->whereYear('created_at', $today->year)
                                                    ->sum('amountconsumed');

                    $amount = $charge->amount * $totalconsumed;

                    $amounts[] = $amount;
                }
  
                
         }

         return array_sum($amounts);

   
    }

    public static function annualchargetotal($id)
    {
        $today = Carbon::today();

       // dd($id);

        $charge = Annualamount::where('annualamounts.chargeid', $id)
                                    ->whereYear('annualamounts.created_at', $today->year)
                                    ->join('charges', 'charges.id', 'annualamounts.chargeid')
                                    ->select('annualamounts.amount', 'charges.chargingtype', 'charges.id as cid')
                                    ->first();
                        //dd($charge->chargingtype);
        if($charge->chargingtype == 1)
        {
           return $charge->amount * Apartment::CondoApartments()->count();
        }else{
             $totalconsumed = Consumptionrate::where('chargeid', $charge->cid)
                                                    ->whereYear('created_at', $today->year)
                                                    ->sum('amountconsumed');

                    return $charge->amount * $totalconsumed;
        }
        
    }



    public static function paidcharges($id)
    {
         $today = Carbon::today();
         $charge = Annualamount::where('annualamounts.chargeid', $id)
                                    ->whereYear('annualamounts.created_at', $today->year)
                                    ->join('charges', 'charges.id', 'annualamounts.chargeid')
                                    ->select('annualamounts.amount', 'charges.chargingtype', 'charges.id as cid')
                                    ->first();

       $paidcharges = Annualpayment::where('chargeid', $id)
                                    ->where('status', 1)
                                    ->whereYear('created_at', $today->year)
                                    ->get();

         if($charge->chargingtype == 1)
        {
           return $charge->amount * $paidcharges->count();
        }else{
             $totalconsumed = Consumptionrate::where('chargeid', $charge->cid)
                                                    ->whereYear('created_at', $today->year)
                                                    ->whereIn('apartmentid', $paidcharges->pluck('apartmentid'))
                                                    ->sum('amountconsumed');

                    return $charge->amount * $totalconsumed;
        }
    }

    public static function totalpaidcharges()
    {
         $today = Carbon::today();

   

         $charges = Annualamount::whereYear('annualamounts.created_at', $today->year)
                                    ->join('charges', 'charges.id', 'annualamounts.chargeid')
                                    ->where('charges.condoid', Auth::user()->id)
                                    ->select('annualamounts.amount', 'charges.chargingtype', 'charges.id as cid')
                                    ->get();

       
       
          $total = array();

         
          foreach($charges as $charge)
         
          {
            if($charge->chargingtype == 1)
        {
            $paidcharges = Annualpayment::where('chargeid', $charge->cid)
                                    ->where('status', 1)
                                    ->whereYear('created_at', $today->year)
                                    ->get();


           $amount = $charge->amount * $paidcharges->count();

           $total[] = $amount;
        }else{
            $paidcharges = Annualpayment::where('chargeid', $charge->cid)
                                    ->where('status', 1)
                                    ->whereYear('created_at', $today->year)
                                    ->get();
                    
             $totalconsumed = Consumptionrate::where('chargeid', $charge->cid)
                                                    ->whereYear('created_at', $today->year)
                                                    ->whereIn('apartmentid', $paidcharges->pluck('apartmentid'))
                                                    ->sum('amountconsumed');

                    $amount = $charge->amount * $totalconsumed;

                    $total[] = $amount;
        }
          }
       return array_sum($total);

    }

     public static function statuscount($status)
  {
    $today = Carbon::today();
   return  Apartment::where('condoid', Auth::user()->id)
                  ->join('annualpayments', 'annualpayments.apartmentid', 'apartments.id')
                  ->whereYear('annualpayments.created_at', $today->year)
                  ->where('annualpayments.status', $status)
                  ->select('apartments.id as aid')
                  ->get()->unique('aid')->count();
  }

  public static function currentyearchargeexpectedincome($chargeid)
  {
        $today = Carbon::today();

    $charge = Annualamount::where('chargeid', $chargeid)
                           ->whereYear('annualamounts.created_at', $today->year)
                           ->join('charges', 'charges.id', 'annualamounts.chargeid', 'amounts.amount')
                           ->first();

     
        if($charge->chargingtype == 1)
        {
          return $charge->amount * Apartment::CondoApartments()->count();
        }
         elseif($charge->chargingtype == 2)
        {
          $consumedamount = Consumptionrate::where('chargeid', $charge->id)
                                           ->whereYear('created_at', $today->year)
                                           ->sum('amountconsumed');

          return $charge->amount * $consumedamount;
        }
     
  }

  public static function currentyearchargepaidincome($chargeid)
  {
    $today = Carbon::today();
      $chargeamount = Annualamount::where('chargeid', $chargeid)
                                  ->whereYear('created_at', $today->year)
                                  ->first();


      $charges = Annualpayment::where('chargeid', $chargeid)
                                   ->whereYear('annualpayments.created_at', $today->year)
                                   ->where('status', 1)
                                   ->join('charges', 'charges.id', 'annualpayments.chargeid')
                                   ->select('charges.id', 'charges.chargetype', 'charges.chargingtype', 'annualpayments.apartmentid')
                                   ->get();

      foreach($charges as $charge)
      {
        if($charge->chargingtype == 1)
        {
          return $chargeamount->amount * $charges->count();
        }
         elseif($charge->chargingtype == 2)
        {
          $consumedamount = Consumptionrate::where('chargeid', $charge->id)
                                           ->whereYear('created_at', $today->year)
                                           ->whereIn('apartmentid', $charges->pluck('apartmentid'))
                                           ->sum('amountconsumed');

          return $chargeamount->amount * $consumedamount;
        }
      }
  }

   public static function totalexpectedincome($chargeid)
  {
        $today = Carbon::today();

    $charge = Annualamount::where('chargeid', $chargeid)
                           ->join('charges', 'charges.id', 'annualamounts.chargeid', 'amounts.amount')
                           ->first();

     
        if($charge->chargingtype == 1)
        {
          return $charge->amount * Apartment::CondoApartments()->count();
        }
         elseif($charge->chargingtype == 2)
        {
          $consumedamount = Consumptionrate::where('chargeid', $charge->id)
                                           ->sum('amountconsumed');

          return $charge->amount * $consumedamount;
        }
     
  }

  public static function totalpaidincome($chargeid)
  {
    $today = Carbon::today();
      $chargeamount = Annualamount::where('chargeid', $chargeid)
                                  ->first();


      $charges = Annualpayment::where('chargeid', $chargeid)
                                   ->where('status', 1)
                                   ->join('charges', 'charges.id', 'annualpayments.chargeid')
                                   ->select('charges.id', 'charges.chargetype', 'charges.chargingtype', 'annualpayments.apartmentid')
                                   ->get();

      foreach($charges as $charge)
      {
        if($charge->chargingtype == 1)
        {
          return $chargeamount->amount * $charges->count();
        }
         elseif($charge->chargingtype == 2)
        {
          $consumedamount = Consumptionrate::where('chargeid', $charge->id)
                                           ->whereIn('apartmentid', $charges->pluck('apartmentid'))
                                           ->sum('amountconsumed');

          return $chargeamount->amount * $consumedamount;
        }
      }
  }

}
