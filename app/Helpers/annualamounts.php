<?php
namespace App\Helpers;

use App\Apartment;

use App\Annualamount;

use App\Charges;

use App\Consumptionrate;

use App\Annualpayment;

use DB;

use Carbon\Carbon;

trait Annualamounts{

public static function calculateamount($apartment_id, $charge)
{
	
	$today = Carbon::today();

	
	if(!isset($charge['status']))
	{
		$getstatus = Annualpayment::where('apartmentid', $apartment_id)
								->where('chargeid', $charge->id)
								->whereYear('created_at', $charge->created_at->year)
								->first();

		 $charge['status'] = $getstatus->status;
	}
	$amount = Annualamount::where('chargeid', $charge->id)
								->whereYear('created_at', $today->year)
								->first();
	if($charge->chargingtype == 1)
	{
		

			return "RM $amount->amount <br/> $charge->status";
	}
	else{
		$consumptionrate = Consumptionrate::where('apartmentid', $apartment_id)
										 ->where('chargeid', $charge->id)
										  ->whereYear('created_at', $today->year)
										  ->first();
			if(is_null($consumptionrate))
			{
				return 'Cosumption Not Recorded';
			}
			else{
				$total = $consumptionrate->amountconsumed * $amount->amount;

						return "RM $total <br/> $charge->status";
 

			}

		
	}			

}

public static function calculateamountwithoutstatus($apartment_id, $charge)
{
	$today = Carbon::today();

	
	if(!isset($charge['status']))
	{
		$getstatus = Monthpayment::where('apartmentid', $apartment_id)
								->where('chargeid', $charge->id)
								->whereMonth('created_at', $charge->created_at->month)
								->first();

		 $charge['status'] = $getstatus->status;

	}

	$amount = Annualamount::where('chargeid', $charge->id)
								->whereYear('created_at', $today->year)
								->first();
	if($charge->chargingtype == 1)
	{
		

			return "RM $amount->amount";
	}
	else{
		$consumptionrate = Consumptionrate::where('apartmentid', $apartment_id)
										 ->where('chargeid', $charge->id)
										  ->whereYear('created_at', $today->year)
										  ->first();

		$total = $consumptionrate->amountconsumed * $amount->amount;

		return "RM $total";
 
	}			
	}

	
}