<?php
namespace App\Helpers;

trait status{

	public function checkpaymentstatus($value)
	{
		if($value == 0){
    		return "<span class='badge' style='background-color:red'>NOT PAID</span>";
    	}elseif($value == 1)
    	{
    		return  "<span class='badge' style='background-color:green'>PAID</span>";
    	}
	}

}