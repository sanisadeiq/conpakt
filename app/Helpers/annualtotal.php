<?php
namespace App\Helpers;

use App\Apartment;

use App\Charges;

use App\Consumptionrate;

use App\Annualamount;

use App\Annualpayment;

use DB;

use Carbon\Carbon;

use Auth;

trait monthtotal{

        

    public function expectedincome()
    {
      $today = Carbon::today();
      $apartments =  Monthpayment::where('status', 1)
                    ->whereMonth('monthpayments.created_at', $today->month)
                    ->join('charges', 'charges.id', 'monthpayments.chargeid')
                    ->where('charges.chargetype', '=', 'Service Charge')
                    ->join('apartments', 'apartments.id', 'monthpayments.apartmentid')
                    ->select('apartments.squarefeet')
                    ->get();

                    
        $chargeamount = Monthamount::whereMonth('monthamounts.created_at', $today->month)
                                    ->join('charges', 'charges.id', 'monthamounts.chargeid')
                                    ->where('charges.chargetype', '=', 'Service Charge')
                                    ->select('monthamounts.amount')
                                    ->first();
                                 
            return $apartments->sum('squarefeet') * $chargeamount->amount;

                
    }

   
  

  


}