<?php
namespace App\Helpers;

use App\Apartment;

use App\Monthamount;

use App\Charges;

use App\Consumptionrate;

use App\Monthpayment;

use DB;

use Carbon\Carbon;

trait amounts{

public static function calculateamount($apartment_id, $charge)
{
	$today = Carbon::today();
	//dd($charge);
	
	if(!isset($charge['status']))
	{
		$getstatus = Monthpayment::where('apartmentid', $apartment_id)
								->where('chargeid', $charge->id)
								->whereMonth('created_at', $charge->created_at->month)
								->first();
							//	var_dump($getstatus);
		 $charge['status'] = $getstatus->status;
		 //dd($charge);
	}

	if($charge->chargetype == 'Service Charge')
	{
		$squarefeet = Apartment::where('id', $apartment_id)->first();

		$costpersquarefeet = Monthamount::where('chargeid', $charge->id)
										->whereMonth('created_at', $charge->created_at->month)
										->first();

		$total = $squarefeet->squarefeet * $costpersquarefeet->amount;

		return "RM $total <br/> $charge->status";

	}
	else{

		$monthamount = Monthamount::whereMonth('monthamounts.created_at', $charge->created_at->month)
						->where('monthamounts.chargeid', $charge->id)
						->first();

		$consumptionrate = Consumptionrate::where('apartmentid', $apartment_id)
						->where('chargeid', $charge->id)
						->whereMonth('created_at', $charge->created_at->month)
						->first();

		$total = $monthamount->amount * $consumptionrate->amountconsumed;

			return "RM $total <br/> $charge->status";
	}
	

}

public static function calculateamountwithoutstatus($apartment_id, $charge)
{
	$today = Carbon::today();

	
	if(!isset($charge['status']))
	{
		$getstatus = Monthpayment::where('apartmentid', $apartment_id)
								->where('chargeid', $charge->id)
								->whereMonth('created_at', $charge->created_at->month)
								->first();

		 $charge['status'] = $getstatus->status;

	}

	if($charge->chargetype == 'Service Charge')
	{
		$squarefeet = Apartment::where('id', $apartment_id)->first();

		$costpersquarefeet = Monthamount::where('chargeid', $charge->id)
										->whereMonth('created_at', $charge->created_at->month)
										->first();

		$total = $squarefeet->squarefeet * $costpersquarefeet->amount;

		return "RM $total";

	}
	else{

		$monthamount = Monthamount::whereMonth('monthamounts.created_at', $charge->created_at->month)
						->where('monthamounts.chargeid', $charge->id)
						->first();

		$consumptionrate = Consumptionrate::where('apartmentid', $apartment_id)
						->where('chargeid', $charge->id)
						->whereMonth('created_at', $charge->created_at->month)
						->first();

		$total = $monthamount->amount * $consumptionrate->amountconsumed;

			return "RM $total";
	}
	

}

public static function reportamounts($apartment_id, $charge, $date)
{
	$today = Carbon::today();

	
	if(!isset($charge['status']))
	{
		$getstatus = Monthpayment::where('apartmentid', $apartment_id)
								->where('chargeid', $charge->id)
								->whereMonth('created_at', $date->month)
								->first();

		 $charge['status'] = $getstatus->status;

	}

	if($charge->chargetype == 'Service Charge')
	{
		$squarefeet = Apartment::where('id', $apartment_id)->first();

		$costpersquarefeet = Monthamount::where('chargeid', $charge->id)
										->whereMonth('created_at', $date->month)
										->first();

		$total = $squarefeet->squarefeet * $costpersquarefeet->amount;

		return "RM $total <br/> $charge->status";

	}
	else{

		$monthamount = Monthamount::whereMonth('monthamounts.created_at', $date->month)
						->where('monthamounts.chargeid', $charge->id)
						->first();

		$consumptionrate = Consumptionrate::where('apartmentid', $apartment_id)
						->where('chargeid', $charge->id)
						->whereMonth('created_at', $date->month)
						->first();

		$total = $monthamount->amount * $consumptionrate->amountconsumed;

			return "RM $total <br/> $charge->status";
	}
	}

}