<?php
namespace App\Http\Traits;

trait fileupload{

public function fileupload($imagename)
{

	  $name = str_random(5);
	  $destinationPath = 'fileuploads/'; // upload path
      $extension = $imagename->getClientOriginalExtension(); // getting image extension
      $fileName = $name.'.'.$extension; // renameing image
      $imagename->move($destinationPath, $fileName); // uploading file to given path
      // sending back with message
     return $fullpath = 'fileuploads/'.$fileName;

}

}