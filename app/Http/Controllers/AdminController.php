<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\lists;

use App\User;

use App\Condominium;

class AdminController extends Controller
{
	use lists;

    public function __construct()
    {
        $this->middleware('auth');
    }

	//this function handles the admin dashboard. Admins are redirected here after login
    
    public function index()
    {
    	//list of states from the lists helpter
    	$states = collect($this->states())->sort();


    	//list of all condominiums registered 
    	$condominia = Condominium::Condolist();


    	
    	return view('admin.index', compact('states', 'condominia'));
    }

    public function addcondominium(Request $request)
    {
    	$user = New User($request->all());

    	$user->password = bcrypt($request['mobilenumber']);

    	$user->usertype = 1;

    	$user->save();

    	$condo = New Condominium($request->all());

    	$condo->loginid = $user->id;

    	$condo->urllink = $request['name'];

    	$condo->save();

    	return back();
    }

    public function condominium($urllink)
    {
    	$infor = Condominium::GetRow($urllink);

    	
    	return view('condominium.index', compact('infor'));
    }
}
