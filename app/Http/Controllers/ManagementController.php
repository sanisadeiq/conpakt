<?php

namespace App\Http\Controllers;

use App\Charges;

use App\Apartment;

use App\consumptionrate;

use Auth;

use App\Monthamount;

use Illuminate\Http\Request;

use DB;

use Carbon\Carbon;

use App\Annualamount;

use App\Helpers\monthtotal;

use App\Monthpayment;

use App\Helpers\paymentrate;

use Khill\Lavacharts\Lavacharts;

class ManagementController extends Controller
{
	use monthtotal;

	use paymentrate;

	public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
    	$today = Carbon::today();

        $debtorlist = Apartment::monthlypaymentlist();

        $overalldebtorlist = Apartment::overallpaymentlist();
    
    	$chargecount = Charges::where('condoid', Auth::user()->id)
    						  ->MonthlyTransactions()
    						  ->join('monthamounts', 'monthamounts.chargeid', 'charges.id')
    						  ->whereMonth('monthamounts.created_at', $today->month)
    						  ->count();

    	$annualchargescount = Charges::where('condoid', Auth::user()->id)
    						  ->AnnualTransactions()
    						  ->join('annualamounts', 'annualamounts.chargeid', 'charges.id')
    						  ->whereYear('annualamounts.created_at', $today->year)
    						  ->count();

    	$annualchargesdefaulterslist = Apartment::annualchargesdefaulterslist();
    	$annualchargescurrentyeardefaulterslist = Apartment::annualchargescurrentyeardefaulterslist();

    	$apartmentcount = Apartment::where('condoid', Auth::user()->id)->count();

    	$charges = Monthamount::whereMonth('monthamounts.created_at', $today->month)
    							->join('charges', 'charges.id', 'monthamounts.chargeid')
    							->where('charges.chargetype', '!=', 'Service Charge')
    							->select('charges.id', 'charges.chargetype', 'monthamounts.amount')
    							->get();
    							//dd($charges);

    	$annualcharges = Annualamount::currentyearcharges()->unique('chargetype');



    	$monthtotalservicechargae = Monthpayment::currentmonthtotalservicecharge();
    			
    	$expectedincome = Monthpayment::otherchargescurrentmonthtotal() + $monthtotalservicechargae;

    	$annualchargescurrentyearexpectedincome = Annualamount::currentyearexpectedincome();

    	$annualchargestotalpaid = Annualamount::totalpaidcharges();
    	//dd($annualchargestotalpaid);


    	$annualchargesbalance = $annualchargescurrentyearexpectedincome - $annualchargestotalpaid;

    	$incomegenerated = $this->totalcollection();

    	$balance = $expectedincome - $incomegenerated;

    	$paidservicecharge = $this->paidservicecharge();

    	$totalservicecharge = $this->servicechargeexpectedincome();

    	$totalpaidservicecharge = $this->totalservicechargepaid();

    	$unpaidservicecharge = $totalservicecharge - $totalpaidservicecharge;

    	$monthpayments = Charges::MonthlyTransactions()
    							->where('chargetype', '!=', 'Service Charge')
    							->where('charges.condoid', Auth::user()->id)
    							->join('monthamounts', 'monthamounts.chargeid', 'charges.id')
    							->whereMonth('monthamounts.created_at', $today->month)
    							->select('charges.id as aid', 'charges.chargetype')
    							->get();
    				//dd($monthpayments);

    	$annualpayments = Charges::AnnualTransactions()
    							->where('charges.condoid', Auth::user()->id)
    							->join('annualamounts', 'annualamounts.chargeid', 'charges.id')
    							->whereYear('annualamounts.created_at', $today->year)
    							->select('charges.id as aid', 'charges.chargetype')
    							->groupBy('charges.id', 'charges.chargetype')
    							->get();
    						
 		$lava = new Lavacharts; 

		$reasons = $lava->DataTable();

		$reasons->addStringColumn('charge Type')
		        ->addNumberColumn('Percent');
		       // dd($charges);
		        foreach($charges as $charge)
		        {
		        	//dd(Charges::specificcharge($charge->id));
		        	$reasons->addRow([$charge->chargetype, Charges::specificcharge($charge->id)]);
		        }
		        $reasons->addRow(['Service Charge', $monthtotalservicechargae]);
		       
		 $Chart = \Lava::PieChart('IMDB', $reasons, [
		    'is3D'   => false   
		]);

		 $lava = new Lavacharts; 

		$acharge = $lava->DataTable();

		$acharge->addStringColumn('charge Type')
		        ->addNumberColumn('Percent');
		        foreach($annualcharges as $charge)
		        {
		        	$acharge->addRow([$charge->chargetype, Annualamount::annualchargetotal($charge->cid)]);
		        }
		       
		       
		 $Chart = \Lava::PieChart('annualchargesdistribution', $acharge, [
		    'is3D'   => false   
		]);

		 $lava = new Lavacharts; 

		$reasons = $lava->DataTable();

		$reasons->addStringColumn('Status')
		        ->addNumberColumn('Percent')
		        ->addRow(['Paid', $incomegenerated])
		        ->addRow(['Not Paid', $balance]);
		       
		 $Chart = \Lava::PieChart('paymentanalysis', $reasons, [
		    'is3D'   => false   
		]);

		  $lava = new Lavacharts; 

		$acharge = $lava->DataTable();

		$acharge->addStringColumn('Status')
		        ->addNumberColumn('Percent')
		        ->addRow(['Paid', $annualchargestotalpaid])
		        ->addRow(['Not Paid', $annualchargesbalance]);
		       
		 $Chart = \Lava::PieChart('annualpaymentanalysis', $acharge, [
		    'is3D'   => false   
		]);

 		$lava = new Lavacharts; 

		$finances = $lava->DataTable();

		$finances->addStringColumn('Charge')
		         ->addNumberColumn('Expected Income')
		         ->addNumberColumn('Captured Income');
		         foreach($charges as $charge)
		         {
		         $finances->addRow([$charge->chargetype, Charges::specificcharge($charge->id), Charges::monthpaidspecificcharge($charge->id)]);
		         }
		         $finances->addRow(['Service Charge', $monthtotalservicechargae, $paidservicecharge]);
		         
		$Chart = \Lava::ColumnChart('Finances', $finances, [
		    'titleTextStyle' => [
		        'color'    => '#eb6b2c',
		        'fontSize' => 14
		    ]
		]);

		$lava = new Lavacharts; 

		$acharge = $lava->DataTable();

		$acharge->addStringColumn('Charge')
		         ->addNumberColumn('Expected Income')
		         ->addNumberColumn('Captured Income');
		         foreach($annualcharges as $charge)
		         {
		         $acharge->addRow([$charge->chargetype, Annualamount::annualchargetotal($charge->cid), Annualamount::paidcharges($charge->cid)]);
		         }
		        
		         
		$Chart = \Lava::ColumnChart('paymentdistribution', $acharge, [
		    'titleTextStyle' => [
		        'color'    => '#eb6b2c',
		        'fontSize' => 14
		    ]
		]);


			$lava = new Lavacharts; 

			$apartments  = $lava->DataTable();

			$apartments->addStringColumn('Payment Status')
			      ->addNumberColumn('Apartments')
			      ->addRow(['Paid',  $this->statuscount(1)])
			      ->addRow(['Defaulters',  $this->statuscount(0)]);
			     

			$Chart = \Lava::BarChart('Numbers', $apartments, [
		    'titleTextStyle' => [
		        'color'    => '#eb6b2c',
		        'fontSize' => 14
		    ]
		]);

		$lava = new Lavacharts; 

			$acharge  = $lava->DataTable();

			$acharge->addStringColumn('Payment Status')
			      ->addNumberColumn('Apartments')
			      ->addRow(['Paid',  Annualamount::statuscount(1)])
			      ->addRow(['Defaulters',  Annualamount::statuscount(0)]);
			     

			$Chart = \Lava::BarChart('annualNumbers', $acharge, [
		    'titleTextStyle' => [
		        'color'    => '#eb6b2c',
		        'fontSize' => 14
		    ]
		]);

		$lava = new Lavacharts; 

		$reasons = $lava->DataTable();

		$reasons->addStringColumn('Paid')
		        ->addNumberColumn('Percent')
		        ->addRow(['Paid',  $paidservicecharge ])
		        ->addRow(['Unpaid', $monthtotalservicechargae - $paidservicecharge]);
		       
		 $Chart = \Lava::PieChart('servicechargepayment', $reasons, [
		    'is3D'   => false   
		]);


		 $lava = new Lavacharts; 

		$reasons = $lava->DataTable();

		$reasons->addStringColumn('Paid')
		        ->addNumberColumn('Percent')
		        ->addRow(['Paid',  $totalpaidservicecharge])
		        ->addRow(['Unpaid',$totalservicecharge - $totalpaidservicecharge]);
		       
		 $Chart = \Lava::PieChart('totalservicecharge', $reasons, [
		    'is3D'   => false   
		]);

    	return view('management.index', compact('chargecount', 'apartmentcount', 'expectedincome', 'anotherchart', 'IMDB', 'Finances', 'debtorlist', 'overalldebtorlist', 'paymentanalysis', 'Numbers', 'servicechargepayment', 'totalservicecharge', 'monthtotalservicechargae', 'monthpayments', 'annualchargescount', 'annualchargescurrentyearexpectedincome', 'annualchargesdistribution', 'paymentdistribution', 'annualpaymentanalysis', 'annualNumbers', 'annualchargesdefaulterslist', 'annualchargescurrentyeardefaulterslist', 'annualpayments'));
    }
}
