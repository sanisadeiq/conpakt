<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Charges;

use App\Monthpayment;

use App\Consumptionrate;

use App\Apartment;

use Carbon\Carbon;

use Auth;

use App\Annualpayment;

use App\Annualamount;

class PaymentController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function monthlypayment()
    {
      $date = Carbon::now();

      $months = Charges::where('condoid', Auth::user()->id)
                       ->join('monthamounts', 'monthamounts.chargeid', 'charges.id')
                       ->groupBy('monthamounts.created_at')
                       ->select('monthamounts.created_at')
                       ->get();

    	return view('payment.monthly', compact('date'));
    }

    public function monthlyconsumption()
    {
    	$date = Carbon::now();

    	return view('payment.monthlyconsumption', compact('date'));
    }

    public function addmonthlyconsumption(Request $request)
    {

    	for($i = 0; $i<=count($request['apartmentid']) - 1; $i++)
    	{
    		$consumptionrate = New Consumptionrate();
    		$consumptionrate->apartmentid = $request['apartmentid'][$i];
    		$consumptionrate->chargeid = $request['chargeid'][$i];
    		$consumptionrate->amountconsumed = $request['amount'][$i];
    		$consumptionrate->save();
    	}

    	return back();
    }

    public function confirmmonthlypayment($id)
    {
    $monthlypayment = Monthpayment::where('monthpayments.id', $id)
                      ->join('charges', 'charges.id', 'monthpayments.chargeid')
                      ->join('apartments', 'apartments.id', 'monthpayments.apartmentid')
                      ->join('apartment_owner_pivots', 'apartment_owner_pivots.apartmentid', 'apartments.id')
                      ->join('users', 'users.id', 'apartment_owner_pivots.ownerid')
                      ->join('owners', 'owners.loginid', 'users.id')
                      ->select('users.name', 'owners.contactaddress', 'apartments.*', 'charges.*', 'charges.id as id', 'apartments.id as aid', 'monthpayments.status')
                      ->first();

        $currentdate = Carbon::today();

        $update = Monthpayment::where('id', $id)->first()->update(['status'    => 1]);

         $pdf = \PDF::loadView('payment.invoice', compact('monthlypayment', 'currentdate'));

        return $pdf->download('invoice.pdf');;

    }

    public function confirmannualpayment($id)
    {
    $monthlypayment = Annualpayment::where('annualpayments.id', $id)
                      ->join('charges', 'charges.id', 'annualpayments.chargeid')
                      ->join('apartments', 'apartments.id', 'annualpayments.apartmentid')
                      ->join('apartment_owner_pivots', 'apartment_owner_pivots.apartmentid', 'apartments.id')
                      ->join('users', 'users.id', 'apartment_owner_pivots.ownerid')
                      ->join('owners', 'owners.loginid', 'users.id')
                      ->select('users.name', 'owners.contactaddress', 'apartments.*', 'charges.*', 'charges.id as id', 'apartments.id as aid', 'annualpayments.status')
                      ->first();

        $currentdate = Carbon::today();

        $update = Annualpayment::where('id', $id)->first()->update(['status'    => 1]);

         $pdf = \PDF::loadView('payment.annualinvoice', compact('monthlypayment', 'currentdate'));

        return $pdf->download('invoice.pdf');;

    }

    public function currentmonthdefaultlist()
    {
        $debtorlist =   Apartment::monthlypaymentlist();

        return view('payment.defaulterslist', compact('debtorlist'));
    }

    public function overallmonthsdefaultlist()
    {
        $debtorlist = Apartment::overallpaymentlist();

        return view('payment.overalldefaulterslist', compact('debtorlist'));

    }

     public function servicechargedefaulterslist()
    {
        $defaulterslist = Charges::where('charges.condoid', Auth::user()->id)
                                      ->where('charges.chargetype', 'Service Charge')
                                     ->join('monthpayments', 'monthpayments.chargeid', 'charges.id')
                                     ->where('monthpayments.status', 0)
                                     ->join('apartments', 'apartments.id', 'monthpayments.apartmentid')
                                    ->select('apartments.id as aid', 'apartments.apartmentnumber', 'monthpayments.created_at', 'charges.id as cid', 'apartments.squarefeet')
                                     ->get();

                        return view('payment.defaultlist', compact('defaulterslist'));
    }

    public function defaultlist($id)
    {
       $defaulterslist = Monthpayment::where('chargeid', $id)
                                ->where('status', 0)
                                ->join('apartments', 'apartments.id', 'monthpayments.apartmentid')
                                ->join('charges', 'charges.id', 'monthpayments.chargeid')
                                ->select('apartments.id as aid', 'apartments.apartmentnumber', 'chargeid as cid', 'monthpayments.id as mid', 'charges.chargetype')
                                ->get();

      return view('payment.chargedefaultlist', compact('defaulterslist'));
    }

    public function annualpayments()
    {
      $date = Carbon::today();

      return view('payment.annually', compact('date'));
    }
     public function annualconsumption()
    {
      $date = Carbon::now();

      return view('payment.annualconsumption', compact('date'));
    }

    public function currentyeardefaulterslist()
    {
      $defaulters = Apartment::annualchargescurrentyeardefaulterslist();

      $title = 'Current Year Defaulters List';

      $currentyearcharges = Annualamount::currentyearcharges();


      return view('payment.annualchargesdefaulters', compact('defaulters', 'title', 'currentyearcharges'));
    }

     public function allyearsdefaulterslist()
    {
      $defaulters = Apartment::annualchargesdefaulterslist();

      $title = 'Overall Years Defaulters List';

      
      return view('payment.annualchargesdefaultersoverall', compact('defaulters', 'title'));
    }

    public function annualpaymentdefaultlist($id)
    {
      $charge = Charges::find($id);

      $defaulters = Apartment::annualchargedefaulterslist($id);

      $title = $charge->chargetype.' '.'Defaulters List';

      
      return view('payment.annualchargesdefaultersoverall', compact('defaulters', 'title'));
    }
}
