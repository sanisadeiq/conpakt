<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Section;

use App\Apartment;

use App\Owner;

use App\User;

use App\Monthpayment;

use App\Apartment_owner_pivot;

use App\Http\Traits\fileupload;

use Auth;

use App\Charges;

use App\Annualpayment;

class ApartmentController extends Controller
{

    use fileupload;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
       
    	return view('apartment.index');
    }

    public function addsection(Request $request)
    {
    	$section = New Section($request->all());

        $section->condoid = Auth::user()->id;

    	$section->save();

    	return back();
    }

    public function addpartment(Request $request)
    {
    	$apartment = New Apartment($request->all());

        $apartment->condoid = Auth::user()->id;

    	$apartment->save();

    	return back();
    }

    public function apartment($id)
    {
    	$info = Apartment::where('apartments.id', '=', $id)
    					->join('sections', 'sections.id', 'apartments.sectionid')
    					->first();
    					
        $ownercheck = Apartment_owner_pivot::Ownercheck($id);

        $charges = Monthpayment::apartmentchargerecord($id);

        $annualcharges = Annualpayment::apartmentchargerecord($id);
                    
    	return view('apartment.apartment', compact('info', 'id', 'ownercheck', 'charges', 'annualcharges'));
    }

    public function newowner(Request $request)
    {
        $user = New User($request->all());
        $user->password = bcrypt($request['contactnumber']);
        $user->usertype = 2;
        $user->save();

        $owner = New Owner($request->all());
        $owner->loginid = $user->id;

        $owner->photograph = $this->fileupload($request['photograph']);

        $owner->save();

        $relationship = New Apartment_owner_pivot();

        $relationship->ownerid = $user->id;

        $relationship->apartmentid =$request['apartmentid'];

        $relationship->save();

        return back();

    }

    public function charges()
    {
        return view('management.charges');
    }

    public function servicecharge(Request $request)
    {
        $charges = New charges();
        $charges->chargetype = 'Service Charge';
        $charges->amount = $request['cost'];
        $charges->condoid = Auth::user()->id;
        $charges->billingtype = $request['billingtype'];
        $charges->chargingtype = $request['chargingtype'];
        $charges->save();

        return back();
    }

    public function othercharges(Request $request)
    {
       // dd($request->all());
        $charges = New Charges($request->all());
        $charges->condoid = Auth::user()->id;
        $charges->save();

        return back();
    }

    public function apartmentslist()
    {
        return view('apartment.list');
    }

    public function directory()
    {
        $owners = User::where('usertype', 2)
                     ->join('owners', 'owners.loginid', 'users.id')
                     ->select('users.id', 'users.name', 'users.email', 'owners.contactnumber')
                     ->get();

        return view('apartment.directory', compact('owners'));
    }
}
