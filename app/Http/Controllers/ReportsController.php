<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Charges;

use Carbon\Carbon;

use Auth;

use App\Monthamount;

use App\Annualamount;

use Illuminate\Support\Facades\Route;


class ReportsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function monthindex(Request $request)
    {
    	 $date = Carbon::now();

      $months = Charges::where('condoid', Auth::user()->id)
                       ->join('monthamounts', 'monthamounts.chargeid', 'charges.id')
                       ->groupBy('monthamounts.created_at', 'monthamounts.id')
                       ->select('monthamounts.created_at', 'monthamounts.id')
                       ->orderBy('id', 'desc')
                       ->get();

            
    	return view('payment.monthly', compact('date', 'months'));
    }

    public function monthreport($id)
    {
      $date = Monthamount::where('id', $id)->first();


       $months = Charges::where('condoid', Auth::user()->id)
                       ->join('monthamounts', 'monthamounts.chargeid', 'charges.id')
                       ->groupBy('monthamounts.created_at', 'monthamounts.id')
                       ->select('monthamounts.created_at', 'monthamounts.id')
                       ->orderBy('id', 'desc')
                       ->get();

            
      return view('payment.months', compact('date', 'months'));
    }

    public function annualindex()
    {
      $date = Carbon::now();

      $years = Charges::where('condoid', Auth::user()->id)
                       ->join('annualamounts', 'annualamounts.chargeid', 'charges.id')
                       ->groupBy('annualamounts.created_at', 'annualamounts.id')
                       ->select('annualamounts.created_at', 'annualamounts.id')
                       ->orderBy('id', 'desc')
                       ->get();

            
      return view('payment.annually', compact('date', 'years'));
    }

    public function annualreport($id)
    {
      $dates = Annualamount::where('id', $id)->first();
      $date = $dates->created_at;

       $years = Charges::where('condoid', Auth::user()->id)
                       ->join('annualamounts', 'annualamounts.chargeid', 'charges.id')
                       ->groupBy('annualamounts.created_at', 'annualamounts.id')
                       ->select('annualamounts.created_at', 'annualamounts.id')
                       ->orderBy('id', 'desc')
                       ->get();
                       //dd($years);
        return view('payment.annually', compact('date', 'years'));
    }
}
