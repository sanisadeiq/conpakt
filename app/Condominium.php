<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condominium extends Model
{
    protected $fillable = [
    	'address',
        'state',
    	'officenumber',
    	'mobilenumber',
    	'contactname',
    	'loginid',
        'urllink'
    ];

    protected $hidden = [
        'loginid'
    ];

     public function setContactNameAttribute($value)
    {
        $this->attributes['contactname'] = title_case($value);
    }

     public function setAddressAttribute($value)
    {
        $this->attributes['address'] = title_case($value);
    }

     public function setStateAttribute($value)
    {
        $this->attributes['state'] = title_case($value);
    }

    public function setUrllinkAttribute($value)
    {
        $this->attributes['urllink'] = str_slug($value);
    }

    public function User()
    {
    	$this->belongs('App\User', 'loginid');
    }

    public function scopeCondolist($query)
    {
        return $query->join('users', 'users.id', 'condominia.loginid')
                        ->select('users.name', 'users.email', 'condominia.address', 'condominia.mobilenumber', 'condominia.officenumber', 'condominia.contactname', 'condominia.state', 'condominia.urllink')
                        ->orderBy('users.name', 'ASC')
                        ->get();
    }

    public function scopeGetRow($query, $urllink)
    {
        return $query->where('condominia.urllink', $urllink)
                        ->join('users', 'users.id', 'condominia.loginid')
                        ->select('users.name', 'users.email', 'condominia.address', 'condominia.mobilenumber', 'condominia.officenumber', 'condominia.contactname', 'condominia.state', 'condominia.urllink')
                        ->orderBy('users.name', 'ASC')
                        ->first();
    }
}
